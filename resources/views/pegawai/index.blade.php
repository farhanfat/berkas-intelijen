@extends('layouts.global')

@section('title-page')
     Data Pegawai
@endsection

@section('starter-page')
    Data Pegawai
@endsection

@section('header')
    <link rel="stylesheet" href=" {{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
 <!-- Main content -->
 <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                                <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
                                <strong>{{ session('status') }}</strong>
                        </div> 
                     @endif
                        <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">Data Pegawai</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                  
                                  <div class="row">
                                  <table id="pengguna" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>NIP</th>
                                        <th>NRP</th>
                                        <th>NIK</th>
                                        <th>No HP</th>
                                        <th>Jabatan</th>
                                        <th>Satker</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    
                                    @forelse ($pegawai as $pegawai)
                                    <tr>
                                        <td>{{ ucfirst($pegawai->nama) }}</td>
                                        <td>{{ ucfirst($pegawai->nip) }}</td>
                                        <td>{{ ucfirst($pegawai->nrp) }}</td>
                                        <td>{{ ucfirst($pegawai->nik) }}</td>
                                        <td>{{ ucfirst($pegawai->no_hp) }}</td>
                                        <td>{{ ucfirst($pegawai->jabatan) }}</td>
                                        <td>{{ ucfirst($pegawai->nama_satker) }}</td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">Tidak ada data</td>
                                        </tr>
                                    @endforelse
                                    
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nama</th>
                                            <th>NIP</th>
                                            <th>NRP</th>
                                            <th>NIK</th>
                                            <th>No HP</th>
                                            <th>Jabatan</th>
                                            <th>Satker</th>
                                        </tr>
                                    </tfoot>
                                  </table>
                              
              </div>
  
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
@endsection


@section('footer')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
        $(function () {
          $("#pengguna").DataTable();
        });
      </script>
@endsection