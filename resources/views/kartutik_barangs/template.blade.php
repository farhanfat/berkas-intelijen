<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);

  if($tanggal == NULL){
      return NULL;
  }
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function bln_indo($req){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  
 
  return $bulan[ (int)$req ];
}

function pecah_kata_depan($req){
  $kalimat = $req;
  $jumlah = "2";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 0, $jumlah));
  return $hasil;
}

function pecah_kata_belakang($req){
  $kalimat = $req;
  $jumlah = "10";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 2, $jumlah));
  return $hasil;
}

function hitung_umur($tanggal_lahir){
	$birthDate = new DateTime($tanggal_lahir);
	$today = new DateTime("today");
	if ($birthDate > $today) { 
	    exit("0 tahun 0 bulan 0 hari");
	}
	$y = $today->diff($birthDate)->y;
	$m = $today->diff($birthDate)->m;
	$d = $today->diff($birthDate)->d;
	return $y." tahun ".$m." bulan ".$d." hari";
}

?>

<table style="width: 100%">
  <tbody>
    <tr style="height: 1px;">
      <td class="tg-c3ow" style="height: 1px; width:2%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:27%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px;">&nbsp;</td>
    </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="6">{{pecah_kata_depan($kartutik[0]->nama_satker)}}</td>
  <th class="tg-dvpl" style="height: 18px; text-align: right;">D.IN.13</th>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="7">{{pecah_kata_belakang($kartutik[0]->nama_satker)}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
      </tr>
  <tr style="height: 18px;">
<td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"><b><u>KARTU TIK BARANG CETAKAN
</u></b>
  </tr>
  <tr style="height: 23.5px;">
  <td class="tg-c3ow "  style="height: 23.5px; text-align: center; " colspan="7">NOMOR : {{$kartutik[0]->nomor}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;" colspan="7">&nbsp;</td>
  
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">I.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">IDENTITAS</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">1.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Nama Barang Cetakan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_barang_cetakan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Penerbit</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->penerbit}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">3.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Pengarang/penanggung jawab</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pengarang}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">4.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Waktu peredaran</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->waktu_peredaran}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">5.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Daerah peredaran</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->daerah_peredaran}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">6.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Pencetak</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pencetakan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">7.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Nama Pimpinan Redaksi</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_pimpinan_redaksi}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">8.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Alamat penerbit</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat_penerbitan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">9.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Alamat pencetakan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat_pencetakan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">10.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Jumlah Oplah</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->jumlah_oplah}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">II.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">BIOGRAFI INTELIJEN</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">1.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Kasus/masalah yang terjadi</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kasus_masalah}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Latar belakang dan akibatnya</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->latar_belakang}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">3.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Tindakan yang dilakukan oleh</td>
    <td class="tg-c3ow" style="height: 18px;"></td>
    <td class="tg-0pky" style="height: 18px;"></td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">a.</td>
    <td class="tg-0pky" style="height: 18px;">Kejaksaan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->tindak_kejaksaan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">b.</td>
    <td class="tg-0pky" style="height: 18px;">Kepolisian</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->tindak_kepolisian}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">c.</td>
    <td class="tg-0pky" style="height: 18px;">Pengadilan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->tindak_pengadilan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">4.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Keterangan lain- lain</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->keterangan_lain}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: left; " colspan="7"></td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: left; " colspan="7">OTENTIKASI</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: left; " colspan="7">(Stempel dan Paraf)</td>
  </tr>
  </tbody>
</table>
  