@extends('layouts.global')

@section('title-page')
Kartu Tik Barang
@endsection

@section('starter-page')
Kartu Tik Barang
@endsection

@section('content')
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        @if (session('status'))
        <div class="alert alert-success alert-dismissible">
          <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
          <strong>{{ session('status') }}</strong>
        </div>
        @endif


        <div class="card card-primary ">
          <div class="card-header">
            <h3 class="card-title">Form Tambah Data </h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" enctype="multipart/form-data" action="{{ route('kartutikbarang.store') }}" method="POST">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <hr size="10px">
                  <center><label for="example-text-input" class="col-sm-6 col-form-label"><b>DRAFT KARTU TIK</b></label></center>
                  <hr size="10px">
                  <iframe id="iframeDocument" src="{{ route('kartutikbarang.template', ['id' => $kartutik[0]->id, 'status' => '']) }}"
                      style="width:100%; min-height: 720px" frameborder="0">
                  </iframe>
                </div>
                <div class="form-group col-md-12">
                  <label for="nomor">Nomor</label>
                  <input type="text" value="{{ $kartutik[0]->nomor}}"
                    class="form-control {{ $errors->first('nomor') ? "is-invalid" : "" }}" id="nomor" name="nomor"
                    placeholder="Masukan Nomor">
                  @if ($errors->first('nomor'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nomor') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>I. IDENTITAS</h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-12">
                  <label for="nama_barang_cetakan">Nama Barang Cetakan</label>
                  <input type="text" value="{{ $kartutik[0]->nama_barang_cetakan}}"
                    class="form-control {{ $errors->first('nama_barang_cetakan') ? "is-invalid" : "" }}" id="nama_barang_cetakan" name="nama_barang_cetakan"
                    placeholder="Masukan Nama Barang Cetakan">
                  @if ($errors->first('nama_barang_cetakan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_barang_cetakan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="penerbit">Penerbit</label>
                  <input type="text" value="{{ $kartutik[0]->penerbit}}"
                    class="form-control {{ $errors->first('penerbit') ? "is-invalid" : "" }}" id="penerbit" name="penerbit"
                    placeholder="Masukan Penerbit">
                  @if ($errors->first('penerbit'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('penerbit') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="pengarang">Pengarang/penanggung jawab</label>
                  <input type="text" value="{{ $kartutik[0]->pengarang}}"
                    class="form-control {{ $errors->first('pengarang') ? "is-invalid" : "" }}" id="pengarang" name="pengarang"
                    placeholder="Masukan Pengarang/penanggung jawab">
                  @if ($errors->first('pengarang'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pengarang') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="waktu_peredaran">Waktu Peredaran</label>
                  <input type="text" value="{{ $kartutik[0]->waktu_peredaran}}"
                    class="form-control {{ $errors->first('waktu_peredaran') ? "is-invalid" : "" }}" id="waktu_peredaran" name="waktu_peredaran"
                    placeholder="Masukan Waktu Peredaran">
                  @if ($errors->first('waktu_peredaran'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('waktu_peredaran') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="daerah_peredaran">Daerah Peredaran</label>
                  <input type="text" value="{{ $kartutik[0]->daerah_peredaran}}"
                    class="form-control {{ $errors->first('daerah_peredaran') ? "is-invalid" : "" }}" id="daerah_peredaran" name="daerah_peredaran"
                    placeholder="Masukan Daerah Peredaran">
                  @if ($errors->first('daerah_peredaran'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('daerah_peredaran') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="pencetakan">Pencetakan</label>
                  <input type="text" value="{{ $kartutik[0]->pencetakan}}"
                    class="form-control {{ $errors->first('pencetakan') ? "is-invalid" : "" }}" id="pencetakan" name="pencetakan"
                    placeholder="Masukan Pencetakan">
                  @if ($errors->first('pencetakan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pencetakan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="nama_pimpinan_redaksi">Nama Pimpinan Redaksi</label>
                  <input type="text" value="{{ $kartutik[0]->nama_pimpinan_redaksi}}"
                    class="form-control {{ $errors->first('nama_pimpinan_redaksi') ? "is-invalid" : "" }}" id="nama_pimpinan_redaksi" name="nama_pimpinan_redaksi"
                    placeholder="Masukan Nama Pimpinan Redaksi">
                  @if ($errors->first('nama_pimpinan_redaksi'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_pimpinan_redaksi') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="alamat_penerbitan">Alamat Penerbitan</label>
                  <input type="text" value="{{ $kartutik[0]->alamat_penerbitan}}"
                    class="form-control {{ $errors->first('alamat_penerbitan') ? "is-invalid" : "" }}" id="alamat_penerbitan" name="alamat_penerbitan"
                    placeholder="Masukan Alamat Penerbitan">
                  @if ($errors->first('alamat_penerbitan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat_penerbitan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="alamat_pencetakan">Alamat Pencetakan</label>
                  <input type="text" value="{{ $kartutik[0]->alamat_pencetakan}}"
                    class="form-control {{ $errors->first('alamat_pencetakan') ? "is-invalid" : "" }}" id="alamat_pencetakan" name="alamat_pencetakan"
                    placeholder="Masukan Alamat Pencetakan">
                  @if ($errors->first('alamat_pencetakan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat_pencetakan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="jumlah_oplah">Jumlah Oplah</label>
                  <input type="text" value="{{ $kartutik[0]->jumlah_oplah}}"
                    class="form-control {{ $errors->first('jumlah_oplah') ? "is-invalid" : "" }}" id="jumlah_oplah" name="jumlah_oplah"
                    placeholder="Masukan Jumlah Oplah">
                  @if ($errors->first('jumlah_oplah'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('jumlah_oplah') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>II.	BIOGRAFI INTELIJEN </h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-12">
                  <label for="kasus_masalah">Kasus/masalah yang terjadi</label>
                  <input type="text" value="{{ $kartutik[0]->kasus_masalah}}"
                    class="form-control {{ $errors->first('kasus_masalah') ? "is-invalid" : "" }}" id="kasus_masalah" name="kasus_masalah"
                    placeholder="Masukan Kasus/masalah yang terjadi">
                  @if ($errors->first('kasus_masalah'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kasus_masalah') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="latar_belakang">Latar belakang dan akibatnya</label>
                  <input type="text" value="{{ $kartutik[0]->latar_belakang}}"
                    class="form-control {{ $errors->first('latar_belakang') ? "is-invalid" : "" }}" id="latar_belakang" name="latar_belakang"
                    placeholder="Masukan Latar belakang dan akibatnya">
                  @if ($errors->first('latar_belakang'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('latar_belakang') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="tindak_kejaksaan">Tindakan yang dilakukan oleh Kejaksaan</label>
                  <input type="text" value="{{ $kartutik[0]->tindak_kejaksaan}}"
                    class="form-control {{ $errors->first('tindak_kejaksaan') ? "is-invalid" : "" }}" id="tindak_kejaksaan" name="tindak_kejaksaan"
                    placeholder="Masukan Tindakan yang dilakukan oleh Kejaksaan">
                  @if ($errors->first('tindak_kejaksaan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tindak_kejaksaan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="tindak_kepolisian">Tindakan yang dilakukan oleh Kepolisian</label>
                  <input type="text" value="{{ $kartutik[0]->tindak_kepolisian}}"
                    class="form-control {{ $errors->first('tindak_kepolisian') ? "is-invalid" : "" }}" id="tindak_kepolisian" name="tindak_kepolisian"
                    placeholder="Masukan Tindakan yang dilakukan oleh Kepolisian">
                  @if ($errors->first('tindak_kepolisian'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tindak_kepolisian') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="tindak_pengadilan">Tindakan yang dilakukan oleh Pengadilan</label>
                  <input type="text" value="{{ $kartutik[0]->tindak_pengadilan}}"
                    class="form-control {{ $errors->first('tindak_pengadilan') ? "is-invalid" : "" }}" id="tindak_pengadilan" name="tindak_pengadilan"
                    placeholder="Masukan Tindakan yang dilakukan oleh Pengadilan">
                  @if ($errors->first('tindak_pengadilan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tindak_pengadilan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="keterangan_lain">Keterangan Lain</label>
                  <input type="text" value="{{ $kartutik[0]->keterangan_lain}}"
                    class="form-control {{ $errors->first('keterangan_lain') ? "is-invalid" : "" }}" id="keterangan_lain" name="keterangan_lain"
                    placeholder="Masukan Keterangan Lain">
                  @if ($errors->first('keterangan_lain'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('keterangan_lain') }}</strong>
                  </div>
                  @endif
                </div>
              </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <input type="text" name="id" value="{{ $kartutik[0]->id }}" hidden>
              <button type="submit" name="save" class="btn btn-success btn-block">Simpan Sementara</button>
              <button type="submit" name="finish" class="btn btn-primary btn-block">Simpan & Finish</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    var xy = 0;
    $("#dynamic-ar").click(function () {
        ++xy;
        $("#dynamicAddRemove").append('<tr id="row'+xy+'"><td><input type="text" name="riwayat_orang_tua[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kawan = 0;
    $("#dynamic-kawan").click(function () {
        ++kawan;
        $("#dynamicAddRemovekawan").append('<tr id="row'+kawan+'"><td><input type="text" name="riwayat_kawan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });

    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var saudara = 0;
    $("#dynamic-saudara").click(function () {
        ++saudara;
        $("#dynamicAddRemoveSaudara").append('<tr id="row'+saudara+'"><td><input type="text" name="daftar_saudara[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kenalan = 0;
    $("#dynamic-kenalan").click(function () {
        ++kenalan;
        $("#dynamicAddRemoveKenalan").append('<tr id="row'+kenalan+'"><td><input type="text" name="daftar_kenalan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });


  });
</script>
@endsection