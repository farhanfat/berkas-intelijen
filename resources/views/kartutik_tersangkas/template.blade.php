<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);

  if($tanggal == NULL){
      return NULL;
  }
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function bln_indo($req){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  
 
  return $bulan[ (int)$req ];
}

function pecah_kata_depan($req){
  $kalimat = $req;
  $jumlah = "2";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 0, $jumlah));
  return $hasil;
}

function pecah_kata_belakang($req){
  $kalimat = $req;
  $jumlah = "10";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 2, $jumlah));
  return $hasil;
}

function hitung_umur($tanggal_lahir){
	$birthDate = new DateTime($tanggal_lahir);
	$today = new DateTime("today");
	if ($birthDate > $today) { 
	    exit("0 tahun 0 bulan 0 hari");
	}
	$y = $today->diff($birthDate)->y;
	$m = $today->diff($birthDate)->m;
	$d = $today->diff($birthDate)->d;
	return $y." tahun ".$m." bulan ".$d." hari";
}

?>

<table style="width: 100%">
  <tbody>
    <tr style="height: 1px;">
      <td class="tg-c3ow" style="height: 1px; width:2%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:27%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px;">&nbsp;</td>
    </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="6">{{pecah_kata_depan($kartutik[0]->nama_satker)}}</td>
  <th class="tg-dvpl" style="height: 18px; text-align: right;">D.IN.15</th>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="7">{{pecah_kata_belakang($kartutik[0]->nama_satker)}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
      </tr>
  <tr style="height: 18px;">
<td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"><b><u>KARTU TIK 
  @if($kartutik[0]->tipe_surat == 1)
  TERSANGKA
  @elseif($kartutik[0]->tipe_surat == 2)
  TERDAKWA
  @elseif($kartutik[0]->tipe_surat == 3)
  TERPIDANA
  @else
  TERSANGKA
  @endif
</u></b>
  </tr>
  <tr style="height: 23.5px;">
  <td class="tg-c3ow "  style="height: 23.5px; text-align: center; " colspan="7">NOMOR : {{$kartutik[0]->nomor}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;" colspan="7">&nbsp;</td>
  
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">I.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">IDENTITAS</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">1.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Nama Lengkap</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_lengkap}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">2.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Nama Samaran / Panggilan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  @if($kartutik[0]->tanggal_lahir != null)
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_panggilan}}</td>
  @else
    <td class="tg-0pky" style="height: 18px;"></td>
  @endif
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">3.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Tempat, Tanggal Lahir (Umur)</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  @if($kartutik[0]->tanggal_lahir != null)
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->tempat_lahir}}, {{tgl_indo($kartutik[0]->tanggal_lahir)}}, {{hitung_umur($kartutik[0]->tanggal_lahir)}}</td>
  @else
    <td class="tg-0pky" style="height: 18px;"></td>
  @endif
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">4.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Jenis Kelamin</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->jenis_kelamin}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">5.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Bangsa / Suku</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->bangsa_suku}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">6.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Kewarganegaraan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kewarganegaraan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">7.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Alamat / Tempat Tinggal</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">8.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Nomor Telepon / HP</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->no_hp}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">9.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">No KTP / Passport</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->no_ktp}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">10.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Agama / Kepercayaan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->agama}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">11.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Pekerjaan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pekerjaan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">12.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Alamat Kantor</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">13.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Status Perkawinan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->status_perkawinan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">14.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Kepartaian</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kepartaian}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">15.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Pendidikan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pendidikan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">II.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">RIWAYAT PERKARA</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">1.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="4">Kejahatan / Pelanggaran yang dilakukan</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">a.</td>
  <td class="tg-0pky" style="height: 18px;">Kasus Posisi Singkat / Pasal yang Dilanggar</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kasus_posisi}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">b.</td>
    <td class="tg-0pky" style="height: 18px;">Latar belakang & akibat peristiwa / kerugian</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->latar_belakang}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">c.</td>
    <td class="tg-0pky" style="height: 18px;">SP3 / SKPP</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @if($kartutik[0]->no_skp != null)
      <td class="tg-0pky" style="height: 18px;">No : {{$kartutik[0]->no_skp}}, Tanggal : {{$kartutik[0]->tanggal_skp}}</td>
    @else
      <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">d.</td>
    <td class="tg-0pky" style="height: 18px;">Putusan Pengadilan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @if($kartutik[0]->putusan_pn != null)
      <td class="tg-0pky" style="height: 18px;">PN : {{$kartutik[0]->putusan_pn}}</td>
    @else
      <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->putusan_pn}}</td>
    @endif
  </tr>
  @if($kartutik[0]->putusan_pt != null)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">PT : {{$kartutik[0]->putusan_pt}}</td>
  </tr>
  @endif
  @if($kartutik[0]->putusan_ma != null)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">MA : {{$kartutik[0]->putusan_ma}}</td>
  </tr>
  @endif
  @forelse($orang_tua as $key => $orang_tuas)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @if($orang_tuas  == $orang_tua[0])
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Nama orang tua / alamat</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @else
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
    <td class="tg-0pky" style="height: 18px;">{{$key+1}}. {{$orang_tuas->nama}}</td>
  </tr>
  @empty
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Nama orang tua / alamat</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
  </tr>
  @endforelse
  @forelse($kawan as $key => $kawans)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @if($kawans  == $kawan[0])
    <td class="tg-c3ow" style="height: 18px;">3.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Nama Kawan yang dikenal</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @else
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
    <td class="tg-0pky" style="height: 18px;">{{$key+1}}. {{$kawans->nama}}</td>
  </tr>
  @empty
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">3.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Nama Kawan yang dikenal</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
  </tr>
  @endforelse
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">4.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">Lain-lain</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->lain_lain}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: left; " colspan="7"></td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: left; " colspan="7">OTENTIKASI</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: left; " colspan="7"></td>
  </tr>
  
  </tbody>
  </table>
  <table style="width: 100%; text-align: center;">
    <tr>
      <td class="tg-c3ow" style="height: 18px; width:20%; "></td>
      <td class="tg-c3ow" style="height: 18px; width:26%; border: 1px solid black;">SIDIK JARI<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></td>
      <td class="tg-c3ow" style="height: 18px; width:26%; border: 1px solid black;">TANDA TANGAN<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></td>
      @if($kartutik[0]->foto != null)
      <td class="tg-c3ow" style="height: 18px; width:26%; border: 1px solid black;"><img border="1" src="{{ asset('storage/foto/'.$kartutik[0]->foto) }}" style="display:block;" width="200" height="200" alt="Foto"></td>
      @else
      <td class="tg-c3ow" style="height: 18px; width:26%; border: 1px solid black;">PHOTO<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></td>
      @endif
    </tr>
  </table>