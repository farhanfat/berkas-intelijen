@extends('layouts.global')

@section('title-page')
Kartu Tik Tersangka
@endsection

@section('starter-page')
Kartu Tik Tersangka
@endsection

@section('content')
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        @if (session('status'))
        <div class="alert alert-success alert-dismissible">
          <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
          <strong>{{ session('status') }}</strong>
        </div>
        @endif


        <div class="card card-primary ">
          <div class="card-header">
            <h3 class="card-title">Form Tambah Data </h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" enctype="multipart/form-data" action="{{ route('kartutiktersangka.store') }}" method="POST">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <hr size="10px">
                  <center><label for="example-text-input" class="col-sm-6 col-form-label"><b>DRAFT KARTU TIK</b></label></center>
                  <hr size="10px">
                  <iframe id="iframeDocument" src="{{ route('kartutiktersangka.template', ['id' => $kartutik[0]->id, 'status' => '']) }}"
                      style="width:100%; min-height: 720px" frameborder="0">
                  </iframe>
                </div>
                <div class="form-group col-md-12">
                  <label for="bidang">Bidang</label>
                    <select class="form-control" name="bidang" id="bidang" required>
                      <option disabled selected value="">-- Pilih --</option>
                      <option value="1" <?php echo $kartutik[0]->bidang == 1 ? 'selected' : '' ?>>Intelijen</option>
                      <option value="2" <?php echo $kartutik[0]->bidang == 2 ? 'selected' : '' ?>>Pidana Umum</option>
                      <option value="3" <?php echo $kartutik[0]->bidang == 3 ? 'selected' : '' ?>>Pidana Khusus</option>
                    </select>
                  @if ($errors->first('bidang'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('bidang') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="tipe_surat">Tipe Surat</label>
                    <select class="form-control" name="tipe_surat" id="tipe_surat" required>
                      <option disabled selected value="">-- Pilih --</option>
                      <option value="1" <?php echo $kartutik[0]->tipe_surat == 1 ? 'selected' : '' ?>>TERSANGKA</option>
                      <option value="2" <?php echo $kartutik[0]->tipe_surat == 2 ? 'selected' : '' ?>>TERDAKWA</option>
                      <option value="3" <?php echo $kartutik[0]->tipe_surat == 3 ? 'selected' : '' ?>>TERPIDANA</option>
                    </select>
                  @if ($errors->first('tipe_surat'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tipe_surat') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="nomor">Nomor</label>
                  <input type="text" value="{{ $kartutik[0]->nomor}}"
                    class="form-control {{ $errors->first('nomor') ? "is-invalid" : "" }}" id="nomor" name="nomor"
                    placeholder="Masukan Nomor Kartu TIK">
                  @if ($errors->first('nomor'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nomor') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>I. IDENTITAS</h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-3">
                  <label for="nama_lengkap">Nama Lengkap</label>
                  <input type="text" value="{{ $kartutik[0]->nama_lengkap }}"
                    class="form-control {{ $errors->first('nama_lengkap') ? "is-invalid" : "" }}" id="nama_lengkap" name="nama_lengkap"
                    placeholder="Masukan Nama Lengkap">
                  @if ($errors->first('nama_lengkap'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_lengkap') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="nama_panggilan">Nama Panggilan</label>
                  <input type="text" value="{{ $kartutik[0]->nama_panggilan }}"
                    class="form-control {{ $errors->first('nama_panggilan') ? "is-invalid" : "" }}" id="nama_panggilan" name="nama_panggilan"
                    placeholder="Masukan Nama Lengkap">
                  @if ($errors->first('nama_panggilan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_panggilan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="tempat_lahir">Tempat Lahir</label>
                  <input type="text" value="{{ $kartutik[0]->tempat_lahir }}"
                    class="form-control {{ $errors->first('tempat_lahir') ? "is-invalid" : "" }}" id="tempat_lahir" name="tempat_lahir"
                    placeholder="Masukan Tempat Lahir">
                  @if ($errors->first('tempat_lahir'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tempat_lahir') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="tanggal_lahir">Tanggal Lahir</label>
                  <input type="date" value="{{ $kartutik[0]->tanggal_lahir }}"
                    class="form-control {{ $errors->first('tanggal_lahir') ? "is-invalid" : "" }}" id="tanggal_lahir" name="tanggal_lahir"
                    placeholder="Masukan Tanggal Lahir">
                  @if ($errors->first('tanggal_lahir'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tanggal_lahir') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <input type="text" value="{{ $kartutik[0]->jenis_kelamin }}"
                    class="form-control {{ $errors->first('jenis_kelamin') ? "is-invalid" : "" }}" id="jenis_kelamin" name="jenis_kelamin"
                    placeholder="Masukan Jenis Kelamin">
                  @if ($errors->first('jenis_kelamin'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('jenis_kelamin') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="bangsa_suku">Bangsa/Suku</label>
                  <input type="text" value="{{ $kartutik[0]->bangsa_suku }}"
                    class="form-control {{ $errors->first('bangsa_suku') ? "is-invalid" : "" }}" id="bangsa_suku" name="bangsa_suku"
                    placeholder="Masukan Bangsa/Suku">
                  @if ($errors->first('bangsa_suku'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('bangsa_suku') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="kewarganegaraan">Kewarganegaraan</label>
                  <input type="text" value="{{ $kartutik[0]->kewarganegaraan }}"
                    class="form-control {{ $errors->first('kewarganegaraan') ? "is-invalid" : "" }}" id="kewarganegaraan" name="kewarganegaraan"
                    placeholder="Masukan Kewarganegaraan">
                  @if ($errors->first('kewarganegaraan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kewarganegaraan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="alamat">Alamat</label>
                  <input type="text" value="{{ $kartutik[0]->alamat }}"
                    class="form-control {{ $errors->first('alamat') ? "is-invalid" : "" }}" id="alamat" name="alamat"
                    placeholder="Masukan Alamat">
                  @if ($errors->first('alamat'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="pendidikan">Pendidikan</label>
                  <input type="text" value="{{ $kartutik[0]->pendidikan }}"
                    class="form-control {{ $errors->first('pendidikan') ? "is-invalid" : "" }}" id="pendidikan" name="pendidikan"
                    placeholder="Masukan Pendidikan">
                  @if ($errors->first('pendidikan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pendidikan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="pekerjaan">Pekerjaan</label>
                  <input type="text" value="{{ $kartutik[0]->pekerjaan }}"
                    class="form-control {{ $errors->first('pekerjaan') ? "is-invalid" : "" }}" id="pekerjaan" name="pekerjaan"
                    placeholder="Masukan Pekerjaan">
                  @if ($errors->first('pekerjaan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pekerjaan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="status_perkawinan">Status Perkawinan</label>
                  <input type="text" value="{{ $kartutik[0]->status_perkawinan }}"
                    class="form-control {{ $errors->first('status_perkawinan') ? "is-invalid" : "" }}" id="status_perkawinan" name="status_perkawinan"
                    placeholder="Masukan Perkawinan">
                  @if ($errors->first('status_perkawinan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('status_perkawinan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="no_ktp">No identitas / KTP</label>
                  <input type="text" value="{{ $kartutik[0]->no_ktp }}"
                    class="form-control {{ $errors->first('no_ktp') ? "is-invalid" : "" }}" id="no_ktp" name="no_ktp"
                    placeholder="Masukan No identitas / KTP">
                  @if ($errors->first('no_ktp'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('no_ktp') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="no_hp">No HP</label>
                  <input type="text" value="{{ $kartutik[0]->no_hp }}"
                    class="form-control {{ $errors->first('no_hp') ? "is-invalid" : "" }}" id="no_hp" name="no_hp"
                    placeholder="Masukan No HP">
                  @if ($errors->first('no_hp'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('no_hp') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="agama">Agama</label>
                  <input type="text" value="{{ $kartutik[0]->agama }}"
                    class="form-control {{ $errors->first('agama') ? "is-invalid" : "" }}" id="agama" name="agama"
                    placeholder="Masukan Agama">
                  @if ($errors->first('agama'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('agama') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="alamat_kantor">Alamat Kantor</label>
                  <input type="text" value="{{ $kartutik[0]->alamat_kantor }}"
                    class="form-control {{ $errors->first('alamat_kantor') ? "is-invalid" : "" }}" id="alamat_kantor" name="alamat_kantor"
                    placeholder="Masukan alamat Kantor">
                  @if ($errors->first('alamat_kantor'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat_kantor') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="kepartaian">Kepartaian</label>
                  <input type="text" value="{{ $kartutik[0]->kepartaian }}"
                    class="form-control {{ $errors->first('kepartaian') ? "is-invalid" : "" }}" id="kepartaian" name="kepartaian"
                    placeholder="Masukan Kepartaian">
                  @if ($errors->first('kepartaian'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kepartaian') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>II. RIWAYAT PERKARA</h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-12">
                  <h5>1. Kejahatan / Pelanggaran yang dilakukan</h5>
                </div>
                <div class="form-group col-md-12">
                  <label for="kasus_posisi">Kasus Posisi Singkat / Pasal yang Dilanggar</label>
                  <input type="text" value="{{ $kartutik[0]->kasus_posisi }}"
                    class="form-control {{ $errors->first('kasus_posisi') ? "is-invalid" : "" }}" id="kasus_posisi" name="kasus_posisi"
                    placeholder="Masukan Kasus Posisi">
                  @if ($errors->first('kasus_posisi'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kasus_posisi') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="latar_belakang">Latar belakang & akibat peristiwa / kerugian </label>
                  <input type="text" value="{{ $kartutik[0]->latar_belakang }}"
                    class="form-control {{ $errors->first('latar_belakang') ? "is-invalid" : "" }}" id="latar_belakang" name="latar_belakang"
                    placeholder="Masukan Latar Belakang">
                  @if ($errors->first('latar_belakang'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('latar_belakang') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="no_skp">No SP3 / SKPP</label>
                  <input type="text" value="{{ $kartutik[0]->no_skp }}"
                    class="form-control {{ $errors->first('no_skp') ? "is-invalid" : "" }}" id="no_skp" name="no_skp"
                    placeholder="Masukan No SP3 / SKPP">
                  @if ($errors->first('no_skp'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('no_skp') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="tanggal_skp">Tanggal SP3 / SKPP</label>
                  <input type="date" value="{{ $kartutik[0]->tanggal_skp }}"
                    class="form-control {{ $errors->first('tanggal_skp') ? "is-invalid" : "" }}" id="tanggal_skp" name="tanggal_skp"
                    placeholder="Masukan Tanggal SP3 / SKPP">
                  @if ($errors->first('tanggal_skp'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tanggal_skp') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="putusan_pn">Putusan PN</label>
                  <input type="text" value="{{ $kartutik[0]->putusan_pn }}"
                    class="form-control {{ $errors->first('putusan_pn') ? "is-invalid" : "" }}" id="putusan_pn" name="putusan_pn"
                    placeholder="Masukan Putusan PN">
                  @if ($errors->first('putusan_pn'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('putusan_pn') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="putusan_pt">Putusan PT</label>
                  <input type="text" value="{{ $kartutik[0]->putusan_pt }}"
                    class="form-control {{ $errors->first('putusan_pt') ? "is-invalid" : "" }}" id="putusan_pt" name="putusan_pt"
                    placeholder="Masukan Putusan PT">
                  @if ($errors->first('putusan_pt'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('putusan_pt') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="putusan_ma">Putusan MA</label>
                  <input type="text" value="{{ $kartutik[0]->putusan_ma }}"
                    class="form-control {{ $errors->first('putusan_ma') ? "is-invalid" : "" }}" id="putusan_ma" name="putusan_ma"
                    placeholder="Masukan Putusan MA">
                  @if ($errors->first('putusan_ma'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('putusan_ma') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <table class="table table-bordered" id="dynamicAddRemove">
                    <tr>
                        <th>Nama Orang Tua / Alamat</th>
                        <th>Action</th>
                    </tr>
                    @forelse($orang_tua as $kt)
                      <tr>
                            <td><input type="text" name="riwayat_orang_tua[]" value="{{$kt->nama}}" placeholder="Masukkan Nama Orang Tua / Alamat" class="form-control" /></td>
                            @if($kt  == $orang_tua[0])
                              <td><button type="button" name="add" id="dynamic-ar" class="btn btn-primary btn-block">Tambah</button></td>
                            @else
                              <td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td>
                            @endif
                      </tr>
                    @empty
                      <tr>
                        <td><input type="text" name="riwayat_orang_tua[]" placeholder="Masukkan Nama Orang Tua / Alamat" class="form-control" /></td>
                        <td><button type="button" name="add" id="dynamic-ar" class="btn btn-primary btn-block">Tambah</button></td>
                      </tr>
                    @endforelse
                  </table>
                </div>
                <div class="form-group col-md-12">
                  <table class="table table-bordered" id="dynamicAddRemovekawan">
                    <tr>
                        <th>Nama Kawan</th>
                        <th>Action</th>
                    </tr>
                    @forelse($kawan as $kt)
                      <tr>
                            <td><input type="text" name="riwayat_kawan[]" value="{{$kt->nama}}" placeholder="Masukkan Nama Orang Tua / Alamat" class="form-control" /></td>
                            @if($kt  == $kawan[0])
                              <td><button type="button" name="add" id="dynamic-kawan" class="btn btn-primary btn-block">Tambah</button></td>
                            @else
                              <td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td>
                            @endif
                      </tr>
                    @empty
                      <tr>
                        <td><input type="text" name="riwayat_kawan[]" placeholder="Masukkan Nama Orang Tua / Alamat" class="form-control" /></td>
                        <td><button type="button" name="add" id="dynamic-kawan" class="btn btn-primary btn-block">Tambah</button></td>
                      </tr>
                    @endforelse
                  </table>
                </div>
                <div class="form-group col-md-12">
                  <label for="lain_lain">Lain lain</label>
                  <input type="text" value="{{ $kartutik[0]->lain_lain }}"
                    class="form-control {{ $errors->first('lain_lain') ? "is-invalid" : "" }}" id="lain_lain" name="lain_lain"
                    placeholder="Masukan Lain lain">
                  @if ($errors->first('lain_lain'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('lain_lain') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <h5>III. Foto & Otentikasi</h5>
                </div>
                <div class="form-group col-md-12">
                  <label for="foto">Upload Foto</label>
                  <input type="file"
                    class="form-control {{ $errors->first('foto') ? "is-invalid" : "" }}" id="foto" name="foto"
                    placeholder="Upload Foto">
                  @if ($errors->first('foto'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('foto') }}</strong>
                  </div>
                  @endif
                </div>
              </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <input type="text" name="id" value="{{ $kartutik[0]->id }}" hidden>
              <button type="submit" name="save" class="btn btn-success btn-block">Simpan Sementara</button>
              <button type="submit" name="finish" class="btn btn-primary btn-block">Simpan & Finish</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    var xy = 0;
    $("#dynamic-ar").click(function () {
        ++xy;
        $("#dynamicAddRemove").append('<tr id="row'+xy+'"><td><input type="text" name="riwayat_orang_tua[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kawan = 0;
    $("#dynamic-kawan").click(function () {
        ++kawan;
        $("#dynamicAddRemovekawan").append('<tr id="row'+kawan+'"><td><input type="text" name="riwayat_kawan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });

    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var saudara = 0;
    $("#dynamic-saudara").click(function () {
        ++saudara;
        $("#dynamicAddRemoveSaudara").append('<tr id="row'+saudara+'"><td><input type="text" name="daftar_saudara[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kenalan = 0;
    $("#dynamic-kenalan").click(function () {
        ++kenalan;
        $("#dynamicAddRemoveKenalan").append('<tr id="row'+kenalan+'"><td><input type="text" name="daftar_kenalan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });


  });
</script>
@endsection