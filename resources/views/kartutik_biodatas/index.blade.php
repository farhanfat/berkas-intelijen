@extends('layouts.global')

@section('title-page')
     Kartu TIK Biodata
@endsection

@section('starter-page')
    Kartu TIK Biodata
@endsection

@section('header')
    <link rel="stylesheet" href=" {{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection
{{$var = 1}}
@section('content')
 <!-- Main content -->
 <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                                <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
                                <strong>{{ session('status') }}</strong>
                        </div> 
                     @endif
                      <a href="{{ route('kartutikbiodata.create') }}" class="btn btn-info" style="margin-bottom: 10px;">Tambah Data</a>
                        <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">Kartu TIK Biodata</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                  <div class="row">
                                  <table id="table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Satker</th>
                                        <th>Nomor</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>Pendidikan</th>
                                        <th>Pekerjaan</th>
                                        <th>Status</th>
                                        <th>Bidang</th>
                                        <th>Lihat</th>
                                        <th>Hapus</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                    @forelse ($kartutik as $kartutik)
                                    
                                    <tr>
                                        <td>{{$var++}}</td>
                                        <td>{{ $kartutik->nama_satker }}</td>
                                        <td>{{ $kartutik->nomor }}</td>
                                        <td>{{ ucfirst($kartutik->nama_lengkap) }}</td>
                                        <td>{{ $kartutik->alamat }}</td>
                                        <td>{{ $kartutik->pendidikan }}</td>
                                        <td>{{ $kartutik->pekerjaan }}</td>
                                        @if($kartutik->status == 2)
                                        <td><button class="btn" style="background: blue;color: white;">DRAFT</button></td>
                                        @elseif($kartutik->status == 3)
                                        <td><button class="btn" style="background: green;color: white;">FINISH</button></td>
                                        @else
                                        <td><button class="btn" style="background: red;color: white;">SALAH</button></td>
                                        @endif
                                        @if($kartutik->bidang == 1)
                                        <td><button class="btn" style="background: green;color: white;">INTEL</button></td>
                                        @elseif($kartutik->bidang == 2)
                                        <td><button class="btn" style="background: red;color: white;">PIDUM</button></td>
                                        @elseif($kartutik->bidang == 3)
                                        <td><button class="btn" style="background: pink;color: white;">PIDSUS</button></td>
                                        @else
                                        <td><button class="btn" style="background: blue;color: white;">-</button></td>
                                        @endif
                                        <td>
                                          <a href="{{ route('kartutikbiodata.edit', [$kartutik->id]) }}" class="btn btn-info"><i class="fa fa-eye"></i> Detail</a>
                                        </td>
                                        <td>
                                          <form 
                                          action="{{ route('kartutikbiodata.destroy', [$kartutik->id]) }}"
                                          onsubmit="return confirm('Hapus data permanen ?')"
                                          method="POST">
                                          @csrf
                                          <input type="hidden" name="_method" value="DELETE">
                                          <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</button>
                                              </form>
                                          </td>
                                    </tr>

                                    @empty
                                        <tr>
                                            <td colspan="5">Tidak ada data</td>
                                        </tr>
                                    @endforelse
                                    
                                    </tbody>
                                  </table>
                              
              </div>
  
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
@endsection


@section('footer')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
        $(function () {
          $("#table").DataTable();
        });
      </script>
@endsection