<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);

  if($tanggal == NULL){
      return NULL;
  }
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function bln_indo($req){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  
 
  return $bulan[ (int)$req ];
}

function pecah_kata_depan($req){
  $kalimat = $req;
  $jumlah = "2";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 0, $jumlah));
  return $hasil;
}

function pecah_kata_belakang($req){
  $kalimat = $req;
  $jumlah = "10";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 2, $jumlah));
  return $hasil;
}

function hitung_umur($tanggal_lahir){
	$birthDate = new DateTime($tanggal_lahir);
	$today = new DateTime("today");
	if ($birthDate > $today) { 
	    exit("0 tahun 0 bulan 0 hari");
	}
	$y = $today->diff($birthDate)->y;
	$m = $today->diff($birthDate)->m;
	$d = $today->diff($birthDate)->d;
	return $y." tahun ".$m." bulan ".$d." hari";
}

?>

<table style="width: 100%">
  <tbody>
    <tr style="height: 1px;">
      <td class="tg-c3ow" style="height: 1px; width:2%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:27%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px;">&nbsp;</td>
    </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="6">{{pecah_kata_depan($kartutik[0]->nama_satker)}}</td>
  <th class="tg-dvpl" style="height: 18px; text-align: right;">D.IN.12</th>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="7">{{pecah_kata_belakang($kartutik[0]->nama_satker)}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
      </tr>
  <tr style="height: 18px;">
<td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"><b>KARTU TIK BIODATA</b></td>
  </tr>
  <tr style="height: 23.5px;">
  <td class="tg-c3ow "  style="height: 23.5px; text-align: center; " colspan="7">NOMOR : {{$kartutik[0]->nomor}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;" colspan="7">&nbsp;</td>
  
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">1.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">IDENTITAS</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">a.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Nama Lengkap</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_lengkap}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">b.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Tempat/Tgl. Lahir/Umur</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  @if($kartutik[0]->tanggal_lahir != null)
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->tempat_lahir}}, {{tgl_indo($kartutik[0]->tanggal_lahir)}}, {{hitung_umur($kartutik[0]->tanggal_lahir)}}</td>
  @else
    <td class="tg-0pky" style="height: 18px;"></td>
  @endif
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">c.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Jenis Kelamin</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->jenis_kelamin}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">d.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Bangsa/Suku</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->bangsa_suku}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">e.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Kewarganegaraan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kewarganegaraan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">f.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Alamat/Tempat Tinggal</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">g.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Pendidikan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pendidikan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">h.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Pekerjaaan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pekerjaan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">i.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Status Perkawinan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->status_pernikahan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">j.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Legitimasi Perkawinan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->legitimasi_perkawinan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">k.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Tempat dan Tgl. Perkawinan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->tempat_perkawinan}}{{', '.$kartutik[0]->tanggal_perkawinan}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">2.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">BIOGRAFI INTELIJEN</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">a.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Riwayat Hidup Singkat</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">1.</td>
  <td class="tg-0pky" style="height: 18px;">Pekerjaan</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->biografi_pekerjaan}}</td>
  </tr>
  @forelse($pendidikan as $key => $pendidikans)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @if($pendidikans  == $pendidikan[0])
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;">Pendidikan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @else
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
    <td class="tg-0pky" style="height: 18px;">{{$key+1}}. {{$pendidikans->histori_pendidikan}}</td>
  </tr>
  @empty
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;">Pendidikan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;"></td>
  </tr>
  @endforelse
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">3.</td>
  <td class="tg-0pky" style="height: 18px;">Kepartaian</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kepartaian}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">4.</td>
  <td class="tg-0pky" style="height: 18px;">Ormas Lainnya</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->ormas_lainnya}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">b.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Keluarga</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">1.</td>
  <td class="tg-0pky" style="height: 18px;">Nama Istri/ Suami</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_istri_suami}}</td>
  </tr>
  @forelse($anak as $key => $anaks)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @if($anaks  == $anak[0])
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;">Nama Anak-anak</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @else
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
    
  <td class="tg-0pky" style="height: 18px;">{{$key+1}}. {{$anaks->nama_anak}}</td>
  </tr>
  @empty
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">2.</td>
  <td class="tg-0pky" style="height: 18px;">Nama Anak-anak</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  </tr>
  @endforelse
  @forelse($saudara as $key => $saudaras)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @if($saudaras  == $saudara[0])
    <td class="tg-c3ow" style="height: 18px;">3.</td>
    <td class="tg-0pky" style="height: 18px;">Nama saudara</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @else
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
    <td class="tg-0pky" style="height: 18px;">{{$key+1}}. {{$saudaras->nama_saudara}}</td>
  </tr>
  @empty
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">3.</td>
  <td class="tg-0pky" style="height: 18px;">Nama Saudara</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  </tr>
  @endforelse
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">4.</td>
  <td class="tg-0pky" style="height: 18px;">Nama Ayah Kandung</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_ayah_kandung}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">Nama Ibu Kandung</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_ibu_kandung}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">Alamat</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat_ayah_ibu}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">5.</td>
  <td class="tg-0pky" style="height: 18px;">Nama Ayah Mertua</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_ayah_mertua}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">Nama Ibu Mertua</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_ibu_mertua}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">Alamat</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->alamat_mertua}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">c.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Referensi/Kenalan</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  </tr>
  @forelse($kenalan as $key => $kenalans)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @if($kenalans  == $kenalan[0])
    <td class="tg-0pky" style="height: 18px;" colspan="2">(nama dan alamat)</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    @else
    <td class="tg-0pky" style="height: 18px;" colspan="2">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    @endif
    <td class="tg-0pky" style="height: 18px;">{{$key+1}}. {{$kenalans->nama_kenalan}}</td>
  </tr>
  @empty
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2">(nama dan alamat)</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;"></td>
  </tr>
  @endforelse
  
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">d.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Hobbi/Kegemaran</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-c3ow" style="height: 18px;">{{$kartutik[0]->hobi_kegemaran}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">e.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Kedudukan dimasyarakat</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-c3ow" style="height: 18px;">{{$kartutik[0]->kedudukan_dimasyarakat}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">f.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="2">Nomor HP</td>
  <td class="tg-c3ow" style="height: 18px;">:</td>
  <td class="tg-c3ow" style="height: 18px;">{{$kartutik[0]->nomor_hp}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">3.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="3">PAS PHOTO</td>
  <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  <td class="tg-dvpl" style="height: 18px;">OTENTIKASI :</td>
  </tr>
  @if($kartutik[0]->foto != null)
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    {{-- <td class="tg-0pky" style="height: 18px; text-align: center; vertical-align: middle;" colspan="3"> --}}
    <td class="tg-0pky" style="height: 18px; text-align:" colspan="3">
      <img border="1" src="{{ asset('storage/foto/'.$kartutik[0]->foto) }}" alt="Foto" width="85px" height="135px">
    </td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;">&nbsp;</td>
  </tr>
  @endif
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
  </tr>
  
  </tbody>
  </table>