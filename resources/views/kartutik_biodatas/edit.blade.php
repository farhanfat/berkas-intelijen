@extends('layouts.global')

@section('title-page')
Kartu TIK Biodata
@endsection

@section('starter-page')
Kartu TIK Biodata
@endsection

@section('content')
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        @if (session('status'))
        <div class="alert alert-success alert-dismissible">
          <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
          <strong>{{ session('status') }}</strong>
        </div>
        @endif


        <div class="card card-primary ">
          <div class="card-header">
            <h3 class="card-title">Form Tambah Data </h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" enctype="multipart/form-data" action="{{ route('kartutikbiodata.store') }}" method="POST">

            @csrf

            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <hr size="10px">
                  <center><label for="example-text-input" class="col-sm-6 col-form-label"><b>DRAFT KARTU TIK</b></label></center>
                  <hr size="10px">
                  <iframe id="iframeDocument" src="{{ route('kartutikbiodata.template', ['id' => $kartutik[0]->id, 'status' => '']) }}"
                      style="width:100%; min-height: 720px" frameborder="0">
                  </iframe>
                </div>
                <div class="form-group col-md-12">
                  <label for="bidang">Bidang</label>
                    <select class="form-control" name="bidang" id="bidang" required>
                      <option disabled selected value="">-- Pilih --</option>
                      <option value="1" <?php echo $kartutik[0]->bidang == 1 ? 'selected' : '' ?>>Intelijen</option>
                      <option value="2" <?php echo $kartutik[0]->bidang == 2 ? 'selected' : '' ?>>Pidana Umum</option>
                      <option value="3" <?php echo $kartutik[0]->bidang == 3 ? 'selected' : '' ?>>Pidana Khusus</option>
                    </select>
                  @if ($errors->first('bidang'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('bidang') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="nomor">Nomor</label>
                  <input type="text" value="{{ $kartutik[0]->nomor}}"
                    class="form-control {{ $errors->first('nomor') ? "is-invalid" : "" }}" id="nomor" name="nomor"
                    placeholder="Masukan Nomor Kartu TIK">
                  @if ($errors->first('nomor'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nomor') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>1. IDENTITAS</h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-6">
                  <label for="nama_lengkap">Nama Lengkap</label>
                  <input type="text" value="{{ $kartutik[0]->nama_lengkap }}"
                    class="form-control {{ $errors->first('nama_lengkap') ? "is-invalid" : "" }}" id="nama_lengkap" name="nama_lengkap"
                    placeholder="Masukan Nama Lengkap">
                  @if ($errors->first('nama_lengkap'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_lengkap') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="tempat_lahir">Tempat Lahir</label>
                  <input type="text" value="{{ $kartutik[0]->tempat_lahir }}"
                    class="form-control {{ $errors->first('tempat_lahir') ? "is-invalid" : "" }}" id="tempat_lahir" name="tempat_lahir"
                    placeholder="Masukan Tempat Lahir">
                  @if ($errors->first('tempat_lahir'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tempat_lahir') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="tanggal_lahir">Tanggal Lahir</label>
                  <input type="date" value="{{ $kartutik[0]->tanggal_lahir }}"
                    class="form-control {{ $errors->first('tanggal_lahir') ? "is-invalid" : "" }}" id="tanggal_lahir" name="tanggal_lahir"
                    placeholder="Masukan Tanggal Lahir">
                  @if ($errors->first('tanggal_lahir'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tanggal_lahir') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <input type="text" value="{{ $kartutik[0]->jenis_kelamin }}"
                    class="form-control {{ $errors->first('jenis_kelamin') ? "is-invalid" : "" }}" id="jenis_kelamin" name="jenis_kelamin"
                    placeholder="Masukan Jenis Kelamin">
                  @if ($errors->first('jenis_kelamin'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('jenis_kelamin') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="bangsa_suku">Bangsa/Suku</label>
                  <input type="text" value="{{ $kartutik[0]->bangsa_suku }}"
                    class="form-control {{ $errors->first('bangsa_suku') ? "is-invalid" : "" }}" id="bangsa_suku" name="bangsa_suku"
                    placeholder="Masukan Bangsa/Suku">
                  @if ($errors->first('bangsa_suku'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('bangsa_suku') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="kewarganegaraan">Kewarganegaraan</label>
                  <input type="text" value="{{ $kartutik[0]->kewarganegaraan }}"
                    class="form-control {{ $errors->first('kewarganegaraan') ? "is-invalid" : "" }}" id="kewarganegaraan" name="kewarganegaraan"
                    placeholder="Masukan Kewarganegaraan">
                  @if ($errors->first('kewarganegaraan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kewarganegaraan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="alamat">Alamat</label>
                  <input type="text" value="{{ $kartutik[0]->alamat }}"
                    class="form-control {{ $errors->first('alamat') ? "is-invalid" : "" }}" id="alamat" name="alamat"
                    placeholder="Masukan Alamat">
                  @if ($errors->first('alamat'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="pendidikan">Pendidikan</label>
                  <input type="text" value="{{ $kartutik[0]->pendidikan }}"
                    class="form-control {{ $errors->first('pendidikan') ? "is-invalid" : "" }}" id="pendidikan" name="pendidikan"
                    placeholder="Masukan Pendidikan">
                  @if ($errors->first('pendidikan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pendidikan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="pekerjaan">Pekerjaan</label>
                  <input type="text" value="{{ $kartutik[0]->pekerjaan }}"
                    class="form-control {{ $errors->first('pekerjaan') ? "is-invalid" : "" }}" id="pekerjaan" name="pekerjaan"
                    placeholder="Masukan Pekerjaan">
                  @if ($errors->first('pekerjaan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pekerjaan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="status_pernikahan">Status Perkawinan</label>
                  <input type="text" value="{{ $kartutik[0]->status_pernikahan }}"
                    class="form-control {{ $errors->first('status_pernikahan') ? "is-invalid" : "" }}" id="status_pernikahan" name="status_pernikahan"
                    placeholder="Masukan Perkawinan">
                  @if ($errors->first('status_pernikahan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('status_pernikahan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="legitimasi_perkawinan">Legistimasi Perkawinan</label>
                  <input type="text" value="{{ $kartutik[0]->legitimasi_perkawinan }}"
                    class="form-control {{ $errors->first('legitimasi_perkawinan') ? "is-invalid" : "" }}" id="legitimasi_perkawinan" name="legitimasi_perkawinan"
                    placeholder="Masukan Legistimasi Perkawinan">
                  @if ($errors->first('legitimasi_perkawinan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('legitimasi_perkawinan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="tempat_perkawinan">Tempat Perkawinan</label>
                  <input type="text" value="{{ $kartutik[0]->tempat_perkawinan }}"
                    class="form-control {{ $errors->first('tempat_perkawinan') ? "is-invalid" : "" }}" id="tempat_perkawinan" name="tempat_perkawinan"
                    placeholder="Masukan Tempat Perkawinan">
                  @if ($errors->first('tempat_perkawinan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tempat_perkawinan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="tanggal_perkawinan">Tanggal Perkawinan</label>
                  <input type="date" value="{{ $kartutik[0]->tanggal_perkawinan }}"
                    class="form-control {{ $errors->first('tanggal_perkawinan') ? "is-invalid" : "" }}" id="tanggal_perkawinan" name="tanggal_perkawinan"
                    placeholder="Masukan Tempat Perkawinan">
                  @if ($errors->first('tanggal_perkawinan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('tanggal_perkawinan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>2. BIOGRAFI INTELIJEN</h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-12">
                  <h5>a. Riwayat Hidup Singkat</h5>
                </div>
                <div class="form-group col-md-12">
                  <label for="biografi_pekerjaan">Pekerjaan</label>
                  <input type="text" value="{{ $kartutik[0]->biografi_pekerjaan }}"
                    class="form-control {{ $errors->first('biografi_pekerjaan') ? "is-invalid" : "" }}" id="biografi_pekerjaan" name="biografi_pekerjaan"
                    placeholder="Masukan Pekerjaan">
                  @if ($errors->first('biografi_pekerjaan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('biografi_pekerjaan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <table class="table table-bordered" id="dynamicAddRemove">
                    <tr>
                        <th>Riwayat Pendidikan</th>
                        <th>Action</th>
                    </tr>
                    @forelse($pendidikan as $kt)
                      <tr>
                            <td><input type="text" name="riwayat_pendidikan[]" value="{{$kt->histori_pendidikan}}" placeholder="Masukkan Riwayat" class="form-control" /></td>
                            @if($kt  == $pendidikan[0])
                              <td><button type="button" name="add" id="dynamic-ar" class="btn btn-primary btn-block">Tambah</button></td>
                            @else
                              <td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td>
                            @endif
                      </tr>
                    @empty
                      <tr>
                        <td><input type="text" name="riwayat_pendidikan[]" placeholder="Masukkan Riwayat" class="form-control" /></td>
                        <td><button type="button" name="add" id="dynamic-ar" class="btn btn-primary btn-block">Tambah</button></td>
                      </tr>
                    @endforelse
                  </table>
                </div>
                <div class="form-group col-md-6">
                  <label for="kepartaian">Partai</label>
                  <input type="text" value="{{ $kartutik[0]->kepartaian }}"
                    class="form-control {{ $errors->first('kepartaian') ? "is-invalid" : "" }}" id="kepartaian" name="kepartaian"
                    placeholder="Masukan Partai">
                  @if ($errors->first('kepartaian'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kepartaian') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="ormas_lainnya">Ormas Lainnya</label>
                  <input type="text" value="{{ $kartutik[0]->ormas_lainnya  }}"
                    class="form-control {{ $errors->first('ormas_lainnya') ? "is-invalid" : "" }}" id="ormas_lainnya" name="ormas_lainnya"
                    placeholder="Masukan Ormas Lainnya">
                  @if ($errors->first('ormas_lainnya'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('ormas_lainnya') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <h5>b. Keluarga</h5>
                </div>
                <div class="form-group col-md-12">
                  <label for="nama_istri_suami">Nama Istri/Suami</label>
                  <input type="text" value="{{ $kartutik[0]->nama_istri_suami  }}"
                    class="form-control {{ $errors->first('nama_istri_suami') ? "is-invalid" : "" }}" id="nama_istri_suami" name="nama_istri_suami"
                    placeholder="Masukan Istri/Suami">
                  @if ($errors->first('nama_istri_suami'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_istri_suami') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <table class="table table-bordered" id="dynamicAddRemoveAnak">
                    <tr>
                        <th>Daftar Anak</th>
                        <th>Action</th>
                    </tr>
                    @forelse($anak as $kt)
                      <tr>
                            <td><input type="text" name="daftar_anak[]" value="{{$kt->nama_anak}}" placeholder="Masukkan Riwayat" class="form-control" /></td>
                            @if($kt  == $anak[0])
                              <td><button type="button" name="add" id="dynamic-anak" class="btn btn-primary btn-block">Tambah</button></td>
                            @else
                              <td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td>
                            @endif
                      </tr>
                    @empty
                      <tr>
                        <td><input type="text" name="daftar_anak[]"  placeholder="Masukkan Riwayat" class="form-control" /></td>
                        <td><button type="button" name="add" id="dynamic-anak" class="btn btn-primary btn-block">Tambah</button></td>
                      </tr>
                    @endforelse
                  </table>
                </div>
                <div class="form-group col-md-12">
                  <table class="table table-bordered" id="dynamicAddRemoveSaudara">
                    <tr>
                        <th>Daftar Saudara</th>
                        <th>Action</th>
                    </tr>
                    @forelse($saudara as $kt)
                    <tr>
                          <td><input type="text" name="daftar_saudara[]" value="{{$kt->nama_saudara}}" placeholder="Masukkan Riwayat" class="form-control" /></td>
                          @if($kt  == $saudara[0])
                            <td><button type="button" name="add" id="dynamic-saudara" class="btn btn-primary btn-block">Tambah</button></td>
                          @else
                            <td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td>
                          @endif
                          
                    </tr>
                    @empty
                    <tr>
                      <td><input type="text" name="daftar_saudara[]" placeholder="Masukkan Riwayat" class="form-control" /></td>
                      <td><button type="button" name="add" id="dynamic-saudara" class="btn btn-primary btn-block">Tambah</button></td>
                    </tr>
                    @endforelse
                  </table>
                </div>
                <div class="form-group col-md-6">
                  <label for="nama_ayah_kandung">Nama Ayah Kandung</label>
                  <input type="text" value="{{ $kartutik[0]->nama_ayah_kandung  }}"
                    class="form-control {{ $errors->first('nama_ayah_kandung') ? "is-invalid" : "" }}" id="nama_ayah_kandung" name="nama_ayah_kandung"
                    placeholder="Masukan Nama Ayah Kandung">
                  @if ($errors->first('nama_ayah_kandung'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_ayah_kandung') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="nama_ibu_kandung">Nama Ibu Kandung</label>
                  <input type="text" value="{{ $kartutik[0]->nama_ibu_kandung }}"
                    class="form-control {{ $errors->first('nama_ibu_kandung') ? "is-invalid" : "" }}" id="nama_ibu_kandung" name="nama_ibu_kandung"
                    placeholder="Masukan Nama Ibu Kandung">
                  @if ($errors->first('nama_ibu_kandung'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_ibu_kandung') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="alamat_ayah_ibu">Alamat Orang Tua</label>
                  <input type="text" value="{{ $kartutik[0]->alamat_ayah_ibu }}"
                    class="form-control {{ $errors->first('alamat_ayah_ibu') ? "is-invalid" : "" }}" id="alamat_ayah_ibu" name="alamat_ayah_ibu"
                    placeholder="Masukan Alamat Orang Tua">
                  @if ($errors->first('alamat_ayah_ibu'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat_ayah_ibu') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="nama_ayah_mertua">Nama Ayah Mertua</label>
                  <input type="text" value="{{ $kartutik[0]->nama_ayah_mertua }}"
                    class="form-control {{ $errors->first('nama_ayah_mertua') ? "is-invalid" : "" }}" id="nama_ayah_mertua" name="nama_ayah_mertua"
                    placeholder="Masukan Nama Ayah Kandung">
                  @if ($errors->first('nama_ayah_mertua'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_ayah_mertua') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="nama_ibu_mertua">Nama Ibu Mertua</label>
                  <input type="text" value="{{ $kartutik[0]->nama_ibu_mertua }}"
                    class="form-control {{ $errors->first('nama_ibu_mertua') ? "is-invalid" : "" }}" id="nama_ibu_mertua" name="nama_ibu_mertua"
                    placeholder="Masukan Nama Ibu Mertua">
                  @if ($errors->first('nama_ibu_mertua'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_ibu_mertua') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="alamat_mertua">Alamat Mertua</label>
                  <input type="text" value="{{ $kartutik[0]->alamat_mertua  }}"
                    class="form-control {{ $errors->first('alamat_mertua') ? "is-invalid" : "" }}" id="alamat_mertua" name="alamat_mertua"
                    placeholder="Masukan Alamat Mertua">
                  @if ($errors->first('alamat_mertua'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('alamat_mertua') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <h5>C. Data Pendukung</h5>
                </div>
                <div class="form-group col-md-12">
                  <table class="table table-bordered" id="dynamicAddRemoveKenalan">
                    <tr>
                        <th>Daftar Kenalan</th>
                        <th>Action</th>
                    </tr>
                      @forelse($kenalan as $kt)
                      <tr>
                        <td><input type="text" name="daftar_kenalan[]" value="{{$kt->nama_kenalan}}" placeholder="Masukkan Kenalan" class="form-control" /></td>
                        @if($kt == $kenalan[0])
                          <td><button type="button" name="add" id="dynamic-kenalan" class="btn btn-primary btn-block">Tambah</button></td>
                        @else
                          <td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td>
                        @endif
                      </tr>
                        @empty
                        <tr>
                          <td><input type="text" name="daftar_kenalan[]" placeholder="Masukkan Kenalan" class="form-control" /></td>
                          <td><button type="button" name="add" id="dynamic-kenalan" class="btn btn-primary btn-block">Tambah</button></td>
                        </tr>
                      @endforelse
                  </table>
                </div>
                <div class="form-group col-md-4">
                  <label for="hobi_kegemaran">Hobi/Kegemaran</label>
                  <input type="text" value="{{ $kartutik[0]->hobi_kegemaran }}"
                    class="form-control {{ $errors->first('hobi_kegemaran') ? "is-invalid" : "" }}" id="hobi_kegemaran" name="hobi_kegemaran"
                    placeholder="Masukan Hobi/Kegemaran">
                  @if ($errors->first('hobi_kegemaran'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('hobi_kegemaran') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="kedudukan_dimasyarakat">Kedudukan Dimasyarakat</label>
                  <input type="text" value="{{ $kartutik[0]->kedudukan_dimasyarakat  }}"
                    class="form-control {{ $errors->first('kedudukan_dimasyarakat') ? "is-invalid" : "" }}" id="kedudukan_dimasyarakat" name="kedudukan_dimasyarakat"
                    placeholder="Masukan Kedudukan Dimasyarakat">
                  @if ($errors->first('kedudukan_dimasyarakat'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kedudukan_dimasyarakat') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="nomor_hp">Nomor HP</label>
                  <input type="text" value="{{ $kartutik[0]->nomor_hp }}"
                    class="form-control {{ $errors->first('nomor_hp') ? "is-invalid" : "" }}" id="nomor_hp" name="nomor_hp"
                    placeholder="Masukan Nomor HP">
                  @if ($errors->first('nomor_hp'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nomor_hp') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <h5>D. Foto & Otentikasi</h5>
                </div>
                <div class="form-group col-md-12">
                  <label for="foto">Upload Foto</label>
                  <input type="file"
                    class="form-control {{ $errors->first('foto') ? "is-invalid" : "" }}" id="foto" name="foto"
                    placeholder="Upload Foto">
                  @if ($errors->first('foto'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('foto') }}</strong>
                  </div>
                  @endif
                </div>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <input type="text" name="id" value="{{ $kartutik[0]->id }}" hidden>
              <button type="submit" name="save" class="btn btn-success btn-block">Simpan Sementara</button>
              <button type="submit" name="finish" class="btn btn-primary btn-block">Simpan & Finish</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    var xy = 0;
    $("#dynamic-ar").click(function () {
        ++xy;
        $("#dynamicAddRemove").append('<tr id="row'+xy+'"><td><input type="text" name="riwayat_pendidikan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var anak = 0;
    $("#dynamic-anak").click(function () {
        ++anak;
        $("#dynamicAddRemoveAnak").append('<tr id="row'+anak+'"><td><input type="text" name="daftar_anak[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var saudara = 0;
    $("#dynamic-saudara").click(function () {
        ++saudara;
        $("#dynamicAddRemoveSaudara").append('<tr id="row'+saudara+'"><td><input type="text" name="daftar_saudara[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kenalan = 0;
    $("#dynamic-kenalan").click(function () {
        ++kenalan;
        $("#dynamicAddRemoveKenalan").append('<tr id="row'+kenalan+'"><td><input type="text" name="daftar_kenalan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });


  });
</script>
@endsection