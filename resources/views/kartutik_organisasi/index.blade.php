@extends('layouts.global')

@section('title-page')
     Kartu TIK Organisasi
@endsection

@section('starter-page')
    Kartu TIK Organisasi
@endsection

@section('header')
    <link rel="stylesheet" href=" {{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection
{{$var = 1}}
@section('content')
 <!-- Main content -->
 <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                                <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
                                <strong>{{ session('status') }}</strong>
                        </div> 
                     @endif
                      <a href="{{ route('kartutikorganisasi.create') }}" class="btn btn-info" style="margin-bottom: 10px;">Tambah Data</a>
                        <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">Kartu TIK Organisasi</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                  <div class="row">
                                  <table id="table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Satker</th>
                                        <th>Nama Organisasi</th>
                                        <th>Akte Pendirian</th>
                                        <th>Kedudukan/status</th>
                                        <th>Berdiri Sejak</th>
                                        <th>Domisili hukum/alamat</th>
                                        <th>Lihat</th>
                                        <th>Hapus</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                    @forelse ($kartutik as $kartutik)
                                    
                                    <tr>
                                        <td>{{$var++}}</td>
                                        <td>{{ $kartutik->nama_satker }}</td>
                                        <td>{{ $kartutik->nama_organisasi }}</td>
                                        <td>{{ $kartutik->akte_pendirian }}</td>
                                        <td>{{ $kartutik->kedudukan }}</td>
                                        <td>{{ $kartutik->berdiri_sejak }}</td>
                                        <td>{{ $kartutik->domisili }}</td>
                                        @if($kartutik->status == 2)
                                        <td><button class="btn" style="background: blue;color: white;">DRAFT</button></td>
                                        @elseif($kartutik->status == 3)
                                        <td><button class="btn" style="background: green;color: white;">FINISH</button></td>
                                        @else
                                        <td><button class="btn" style="background: red;color: white;">SALAH</button></td>
                                        @endif
                                        <td>
                                          <a href="{{ route('kartutikorganisasi.edit', [$kartutik->id]) }}" class="btn btn-info"><i class="fa fa-eye"></i> Detail</a>
                                        </td>
                                        <td>
                                          <form 
                                          action="{{ route('kartutikorganisasi.destroy', [$kartutik->id]) }}"
                                          onsubmit="return confirm('Hapus data permanen ?')"
                                          method="POST">
                                          @csrf
                                          <input type="hidden" name="_method" value="DELETE">
                                          <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</button>
                                              </form>
                                          </td>
                                    </tr>

                                    @empty
                                        <tr>
                                            <td colspan="8">Tidak ada data</td>
                                        </tr>
                                    @endforelse
                                    
                                    </tbody>
                                  </table>
                              
              </div>
  
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
@endsection


@section('footer')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
        $(function () {
          $("#table").DataTable();
        });
      </script>
@endsection