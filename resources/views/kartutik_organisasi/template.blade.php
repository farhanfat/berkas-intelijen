<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);

  if($tanggal == NULL){
      return NULL;
  }
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function bln_indo($req){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  
 
  return $bulan[ (int)$req ];
}

function pecah_kata_depan($req){
  $kalimat = $req;
  $jumlah = "2";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 0, $jumlah));
  return $hasil;
}

function pecah_kata_belakang($req){
  $kalimat = $req;
  $jumlah = "10";
  $hasil = implode(" ", array_slice(explode(" ", $kalimat), 2, $jumlah));
  return $hasil;
}

function hitung_umur($tanggal_lahir){
	$birthDate = new DateTime($tanggal_lahir);
	$today = new DateTime("today");
	if ($birthDate > $today) { 
	    exit("0 tahun 0 bulan 0 hari");
	}
	$y = $today->diff($birthDate)->y;
	$m = $today->diff($birthDate)->m;
	$d = $today->diff($birthDate)->d;
	return $y." tahun ".$m." bulan ".$d." hari";
}

?>

<table style="width: 100%">
  <tbody>
    <tr style="height: 1px;">
      <td class="tg-c3ow" style="height: 1px; width:2%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:4%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:27%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px; width:3%;">&nbsp;</td>
      <td class="tg-c3ow" style="height: 1px;">&nbsp;</td>
    </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="6">{{pecah_kata_depan($kartutik[0]->nama_satker)}}</td>
  <th class="tg-dvpl" style="height: 18px; text-align: right;">R.IN.34</th>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-0pky" style="height: 18px;" colspan="7">{{pecah_kata_belakang($kartutik[0]->nama_satker)}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"></td>
      </tr>
  <tr style="height: 18px;">
<td class="tg-9353" style="height: 18px; text-align: center; " colspan="7"><b><u>KARTU TIK ORGANISASI
</u></b>
  </tr>
  <tr style="height: 23.5px;">
  <td class="tg-c3ow "  style="height: 23.5px; text-align: center; " colspan="7">NOMOR : {{$kartutik[0]->nomor}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;" colspan="7">&nbsp;</td>
  
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">I.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">IDENTITAS</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">1.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Nama Organisasi</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nama_organisasi}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">2.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Akte Pendirian</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->akte_pendirian}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">3.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Kedudukan/status</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->kedudukan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">4.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Berdiri Sejak</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->berdiri_sejak}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">5.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Domisili hukum/alamat</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->domisili}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">6.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Nomor Telpon</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->nomor_telepon}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">7.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Website/ E-mail</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->website_email}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">8.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Pengurus</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">- Nama</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pengurus_nama}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">- Kedudukan</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pengurus_kedudukan}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">- Periode Tahun</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pengurus_periode}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">- Alamat</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pengurus_alamat}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">- Nomor Telpon/ HP</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->pengurus_nomor}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">9.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Ruang lingkup kegiatan organisasi</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">a. Kedalam</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->ruang_lingkup_kedalam}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">b. Keluar</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->ruang_lingkup_keluar}}</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">10.</td>
    <td class="tg-0pky" style="height: 18px;" colspan="2" style="width:5%">Jumlah Oplah</td>
    <td class="tg-c3ow" style="height: 18px;">:</td>
    <td class="tg-0pky" style="height: 18px;">{{$kartutik[0]->jumlah_oplah}}</td>
  </tr>
  <tr style="height: 18px;">
  <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
  <td class="tg-c3ow" style="height: 18px;">II.</td>
  <td class="tg-0pky" style="height: 18px;" colspan="5">KEGIATAN ORGANISASI / PENGURUS ORGANISASI YANG BERKAITAN DENGAN PELANGARAN HUKUM</td>
  </tr>
  <tr style="height: 18px;">
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-c3ow" style="height: 18px;">&nbsp;</td>
    <td class="tg-0pky" style="height: 18px;" colspan="5">{{$kartutik[0]->kegiatan_organisasi}}</td>
  </tr>
  </tbody>
</table>
  