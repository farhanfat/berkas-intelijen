@extends('layouts.global')

@section('title-page')
Kartu Tik Organisasi
@endsection

@section('starter-page')
Kartu Tik Organisasi
@endsection

@section('content')
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        @if (session('status'))
        <div class="alert alert-success alert-dismissible">
          <h6><i class="icon fa fa-ban"></i> Pesan !</h6>
          <strong>{{ session('status') }}</strong>
        </div>
        @endif


        <div class="card card-primary ">
          <div class="card-header">
            <h3 class="card-title">Form Tambah Data </h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" enctype="multipart/form-data" action="{{ route('kartutikorganisasi.store') }}" method="POST">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <hr size="10px">
                  <center><label for="example-text-input" class="col-sm-6 col-form-label"><b>DRAFT KARTU TIK</b></label></center>
                  <hr size="10px">
                  <iframe id="iframeDocument" src="{{ route('kartutikorganisasi.template', ['id' => $kartutik[0]->id, 'status' => '']) }}"
                      style="width:100%; min-height: 720px" frameborder="0">
                  </iframe>
                </div>
                <div class="form-group col-md-12">
                  <label for="nomor">Nomor</label>
                  <input type="text" value="{{ $kartutik[0]->nomor}}"
                    class="form-control {{ $errors->first('nomor') ? "is-invalid" : "" }}" id="nomor" name="nomor"
                    placeholder="Masukan Nomor">
                  @if ($errors->first('nomor'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nomor') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <hr size="10px">
                  <h4>I. IDENTITAS</h4>
                  <hr size="10px">
                </div>
                <div class="form-group col-md-12">
                  <label for="nama_organisasi">Nama Organisasi</label>
                  <input type="text" value="{{ $kartutik[0]->nama_organisasi}}"
                    class="form-control {{ $errors->first('nama_organisasi') ? "is-invalid" : "" }}" id="nama_organisasi" name="nama_organisasi"
                    placeholder="Masukan Nama Organisasi">
                  @if ($errors->first('nama_organisasi'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nama_organisasi') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="akte_pendirian">Akte Pendirian</label>
                  <input type="text" value="{{ $kartutik[0]->akte_pendirian}}"
                    class="form-control {{ $errors->first('akte_pendirian') ? "is-invalid" : "" }}" id="akte_pendirian" name="akte_pendirian"
                    placeholder="Masukan Akte Pendirian">
                  @if ($errors->first('akte_pendirian'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('akte_pendirian') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="kedudukan">Kedudukan Status</label>
                  <input type="text" value="{{ $kartutik[0]->kedudukan}}"
                    class="form-control {{ $errors->first('kedudukan') ? "is-invalid" : "" }}" id="kedudukan" name="kedudukan"
                    placeholder="Masukan Kedudukan Status">
                  @if ($errors->first('kedudukan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kedudukan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="berdiri_sejak">Berdiri Sejak</label>
                  <input type="text" value="{{ $kartutik[0]->berdiri_sejak}}"
                    class="form-control {{ $errors->first('berdiri_sejak') ? "is-invalid" : "" }}" id="berdiri_sejak" name="berdiri_sejak"
                    placeholder="Masukan Berdiri Sejak">
                  @if ($errors->first('berdiri_sejak'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('berdiri_sejak') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="domisili">Domisili hukum/alamat</label>
                  <input type="text" value="{{ $kartutik[0]->domisili}}"
                    class="form-control {{ $errors->first('domisili') ? "is-invalid" : "" }}" id="domisili" name="domisili"
                    placeholder="Masukan Domisili hukum/alamat">
                  @if ($errors->first('domisili'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('domisili') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="nomor_telepon">Nomor Telepon</label>
                  <input type="text" value="{{ $kartutik[0]->nomor_telepon}}"
                    class="form-control {{ $errors->first('nomor_telepon') ? "is-invalid" : "" }}" id="nomor_telepon" name="nomor_telepon"
                    placeholder="Masukan Nomor Telepon">
                  @if ($errors->first('nomor_telepon'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('nomor_telepon') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="website_email">Website / Email</label>
                  <input type="text" value="{{ $kartutik[0]->website_email}}"
                    class="form-control {{ $errors->first('website_email') ? "is-invalid" : "" }}" id="website_email" name="website_email"
                    placeholder="Masukan Website / Email">
                  @if ($errors->first('website_email'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('website_email') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="pengurus_nama">Nama Pengurus</label>
                  <input type="text" value="{{ $kartutik[0]->pengurus_nama}}"
                    class="form-control {{ $errors->first('pengurus_nama') ? "is-invalid" : "" }}" id="pengurus_nama" name="pengurus_nama"
                    placeholder="Masukan Nama Pengurus">
                  @if ($errors->first('pengurus_nama'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pengurus_nama') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="pengurus_kedudukan">Kedudukan Pengurus</label>
                  <input type="text" value="{{ $kartutik[0]->pengurus_kedudukan}}"
                    class="form-control {{ $errors->first('pengurus_kedudukan') ? "is-invalid" : "" }}" id="pengurus_kedudukan" name="pengurus_kedudukan"
                    placeholder="Masukan Kedudukan Pengurus">
                  @if ($errors->first('pengurus_kedudukan'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pengurus_kedudukan') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="pengurus_periode">Periode Tahun Pengurus</label>
                  <input type="text" value="{{ $kartutik[0]->pengurus_periode}}"
                    class="form-control {{ $errors->first('pengurus_periode') ? "is-invalid" : "" }}" id="pengurus_periode" name="pengurus_periode"
                    placeholder="Masukan Periode Tahun Pengurus">
                  @if ($errors->first('pengurus_periode'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pengurus_periode') }}</strong>
                  </div>
                  @endif
                </div>
                
                <div class="form-group col-md-6">
                  <label for="pengurus_nomor">Nomor Telepon Pengurus</label>
                  <input type="text" value="{{ $kartutik[0]->pengurus_nomor}}"
                    class="form-control {{ $errors->first('pengurus_nomor') ? "is-invalid" : "" }}" id="pengurus_nomor" name="pengurus_nomor"
                    placeholder="Masukan Nomor Telepon Pengurus">
                  @if ($errors->first('pengurus_nomor'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pengurus_nomor') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="pengurus_alamat">Alamat Pengurus</label>
                  <input type="text" value="{{ $kartutik[0]->pengurus_alamat}}"
                    class="form-control {{ $errors->first('pengurus_alamat') ? "is-invalid" : "" }}" id="pengurus_alamat" name="pengurus_alamat"
                    placeholder="Masukan Alamat Pengurus">
                  @if ($errors->first('pengurus_alamat'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('pengurus_alamat') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="ruang_lingkup_kedalam">Ruang Lingkup Kedalam pada Kegiatan Organisasi</label>
                  <input type="textarea" value="{{ $kartutik[0]->ruang_lingkup_kedalam}}"
                    class="form-control {{ $errors->first('ruang_lingkup_kedalam') ? "is-invalid" : "" }}" id="ruang_lingkup_kedalam" name="ruang_lingkup_kedalam"
                    placeholder="Masukan Ruang Lingkup Kedalam pada Kegiatan Organisasi">
                  @if ($errors->first('ruang_lingkup_kedalam'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('ruang_lingkup_kedalam') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="ruang_lingkup_keluar">Ruang Lingkup Keluar pada Kegiatan Organisasi</label>
                  <input type="textarea" value="{{ $kartutik[0]->ruang_lingkup_keluar}}"
                    class="form-control {{ $errors->first('ruang_lingkup_keluar') ? "is-invalid" : "" }}" id="ruang_lingkup_keluar" name="ruang_lingkup_keluar"
                    placeholder="Masukan Ruang Lingkup Keluar pada Kegiatan Organisasi">
                  @if ($errors->first('ruang_lingkup_keluar'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('ruang_lingkup_keluar') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="jumlah_oplah">Jumlah Oplah</label>
                  <input type="text" value="{{ $kartutik[0]->jumlah_oplah}}"
                    class="form-control {{ $errors->first('jumlah_oplah') ? "is-invalid" : "" }}" id="jumlah_oplah" name="jumlah_oplah"
                    placeholder="Masukan Jumlah Oplah">
                  @if ($errors->first('jumlah_oplah'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('jumlah_oplah') }}</strong>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-12">
                  <label for="kegiatan_organisasi">KEGIATAN ORGANISASI</label>
                  <input type="textarea" value="{{ $kartutik[0]->kegiatan_organisasi}}"
                    class="form-control {{ $errors->first('kegiatan_organisasi') ? "is-invalid" : "" }}" id="kegiatan_organisasi" name="kegiatan_organisasi"
                    placeholder="Masukan kegiatan organisasi">
                  @if ($errors->first('kegiatan_organisasi'))
                  <div class="alert alert-danger alert-dismissible" style="margin-top:15px;">
                    <strong><i class="icon fa fa-ban"></i> {{ $errors->first('kegiatan_organisasi') }}</strong>
                  </div>
                  @endif
                </div>
              </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <input type="text" name="id" value="{{ $kartutik[0]->id }}" hidden>
              <button type="submit" name="save" class="btn btn-success btn-block">Simpan Sementara</button>
              <button type="submit" name="finish" class="btn btn-primary btn-block">Simpan & Finish</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    var xy = 0;
    $("#dynamic-ar").click(function () {
        ++xy;
        $("#dynamicAddRemove").append('<tr id="row'+xy+'"><td><input type="text" name="riwayat_orang_tua[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kawan = 0;
    $("#dynamic-kawan").click(function () {
        ++kawan;
        $("#dynamicAddRemovekawan").append('<tr id="row'+kawan+'"><td><input type="text" name="riwayat_kawan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });

    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var saudara = 0;
    $("#dynamic-saudara").click(function () {
        ++saudara;
        $("#dynamicAddRemoveSaudara").append('<tr id="row'+saudara+'"><td><input type="text" name="daftar_saudara[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    var kenalan = 0;
    $("#dynamic-kenalan").click(function () {
        ++kenalan;
        $("#dynamicAddRemoveKenalan").append('<tr id="row'+kenalan+'"><td><input type="text" name="daftar_kenalan[]" placeholder="Masukkan Nama" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });


  });
</script>
@endsection