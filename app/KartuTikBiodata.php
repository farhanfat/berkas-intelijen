<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KartuTikBiodata extends Model
{    
    use SoftDeletes;

    protected $table = 'kartutik_biodatas';

    protected $primaryKey = 'id';
}
