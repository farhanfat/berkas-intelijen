<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KartuTikOrganisasi extends Model
{    
    use SoftDeletes;

    protected $table = 'kartutik_organisasi';

    protected $primaryKey = 'id';
}
