<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KartuTikTersangka extends Model
{    
    use SoftDeletes;

    protected $table = 'kartutik_tersangkas';

    protected $primaryKey = 'id';
}
