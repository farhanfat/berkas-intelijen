<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KartuTikBarang extends Model
{    
    use SoftDeletes;

    protected $table = 'kartutik_barangs';

    protected $primaryKey = 'id';
}
