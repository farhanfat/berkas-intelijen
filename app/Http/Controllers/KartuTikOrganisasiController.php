<?php

namespace App\Http\Controllers;

use App\KartuTikBarang;
use App\KartuTikOrganisasi;
use Carbon\Carbon;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;

class KartuTikOrganisasiController extends Controller
{

    public function __construct()
    {
        $this->middleware(function($request, $next){

            if(Gate::allows('manage-pegawai')) return $next($request);
          
            abort(403, 'Anda tidak memiliki cukup hak akses');
          });
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        if (auth()->user()->kode_satker == null){
            $kartutik = KartuTikOrganisasi::leftJoin('satkers','kartutik_organisasi.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_organisasi.*','satkers.nama_satker as nama_satker')
            ->whereIn('kartutik_organisasi.status',[2,3])
            ->get();
        }else{
            $kartutik = KartuTikOrganisasi::leftJoin('satkers','kartutik_organisasi.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_organisasi.*','satkers.nama_satker as nama_satker')
            ->where('kartutik_organisasi.kode_satker',auth()->user()->kode_satker)
            ->whereIn('kartutik_organisasi.status',[2,3])
            ->get();
        }

        return view('kartutik_organisasi.index',compact('kartutik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $KartuTikOrganisasi = new KartuTikOrganisasi();
        $KartuTikOrganisasi->status = '1';
        $KartuTikOrganisasi->kode_satker = auth()->user()->kode_satker;
        $KartuTikOrganisasi->save();

        $kartutik = KartuTikOrganisasi::leftJoin('satkers','kartutik_organisasi.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_organisasi.id',$KartuTikOrganisasi->id)
        ->select('kartutik_organisasi.*','satkers.nama_satker as nama_satker')
        ->get();

        return view('kartutik_organisasi.create', compact('kartutik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        if($request->has('save')) {
            $update = KartuTikOrganisasi::find($request->id);
            $update->nomor = $request->nomor;
            $update->nama_organisasi = $request->nama_organisasi;
            $update->kedudukan = $request->kedudukan;
            $update->berdiri_sejak = $request->berdiri_sejak;
            $update->akte_pendirian = $request->akte_pendirian;
            $update->domisili = $request->domisili;
            $update->nomor_telepon = $request->nomor_telepon;
            $update->website_email = $request->website_email;
            $update->jumlah_oplah = $request->jumlah_oplah;
            $update->pengurus_nama = $request->pengurus_nama;
            $update->pengurus_kedudukan = $request->pengurus_kedudukan;
            $update->pengurus_periode = $request->pengurus_periode;
            $update->pengurus_nomor = $request->pengurus_nomor;
            $update->pengurus_alamat = $request->pengurus_alamat;
            $update->ruang_lingkup_kedalam = $request->ruang_lingkup_kedalam;
            $update->ruang_lingkup_keluar = $request->ruang_lingkup_keluar;
            $update->kegiatan_organisasi = $request->kegiatan_organisasi;
            $update->status = '2';
            $update->update();

            
            return $this->edit($request->id);

        
        }elseif($request->has('finish')){
            $update = KartuTikOrganisasi::find($request->id);
            $update->status = '3';
            $update->update();
            return $this->edit($request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kartutik = KartuTikOrganisasi::leftJoin('kartutik_organisasi_anaks','kartutik_organisasi.id','=','kartutik_organisasi_anaks.id_kartutik_organisasi')
        ->leftJoin('kartutik_organisasi_kenalan','kartutik_organisasi.id','=','kartutik_organisasi_kenalan.id_kartutik_organisasi')
        ->leftJoin('kartutik_organisasi_pendidikans','kartutik_organisasi.id','=','kartutik_organisasi_pendidikans.id_kartutik_organisasi')
        ->leftJoin('kartutik_organisasi_saudara','kartutik_organisasi.id','=','kartutik_organisasi_saudara.id_kartutik_organisasi')
        ->leftJoin('satkers','kartutik_organisasi.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_organisasi.id', $id)
        ->select('kartutik_organisasi.*','kartutik_organisasi_anaks.nama_anak as nama_anak','kartutik_organisasi_kenalan.nama_kenalan as nama_kenalan','kartutik_organisasi_pendidikans.histori_pendidikan as histori_pendidikan','satkers.nama_satker as nama_satker' ,'kartutik_organisasi_saudara.nama_saudara as nama_saudara')
        ->get();

        return view('kartutik_organisasi.update', compact('kartutik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $kartutik = KartuTikOrganisasi::leftJoin('satkers','kartutik_organisasi.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_organisasi.id',$id)
        ->select('kartutik_organisasi.*','satkers.nama_satker as nama_satker')
        ->get();

    

        return view('kartutik_organisasi.edit', compact('kartutik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->get('status');
        $order->save();
        return redirect()->route('kartutik_organisasi.index', [$order->id])->with('status','Status Order Berhasil Di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kartutik = KartuTikOrganisasi::findOrFail($id);
        $kartutik->delete();
        return Redirect::back()->with('message','Operation Successful !');
    }

    public function template(Request $request, $status, $filename = '')
    {


        $kartutik = KartuTikOrganisasi::leftJoin('satkers','kartutik_organisasi.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_organisasi.id',$request->id)
        ->select('kartutik_organisasi.*','satkers.nama_satker as nama_satker')
        ->get();

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];		
		$defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
		
        $kertas = 'Folio';
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                'arial-cufonfonts',
            ]),
            'fontdata' => $fontData + [
                'arial' => [
                    'R' => 'ARIAL.TTF'
                ]
            ],
            'orientation' => 'P',
            'default_font_size' => 12,
            'margin_top' => 2,
            'margin_bottom' => 0,
            'margin_left' => 10,
            'margin_right' => 10,
            'mode' => 'utf-8', 
            'format' => $kertas
        ]);

        $mpdf->WriteHTML(\View::make("kartutik_organisasi.template")->with(compact('kartutik')));
        // return view('kartutik_organisasi.template', compact('kartutik','kenalan','pendidikan','anak','saudara'));

        if (empty($filename)) {
            if (!$request->encode) {
                $filename =  'kartutik_biodata.pdf';
                $mpdf->Output($filename, 'I'); 
            }
            else {
                return base64_encode($mpdf->Output('', 'S'));
            } 
        }
        else {

            $url_upload = $this->pathStoragePangkat.'/'.$filename;
            $mpdf->Output($url_upload, 'F');
            
        }
    }
}
