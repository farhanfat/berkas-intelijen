<?php

namespace App\Http\Controllers;

use App\KartuTikTersangka;
use Carbon\Carbon;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;

class KartuTikTersangkaController extends Controller
{

    public function __construct()
    {
        $this->middleware(function($request, $next){

            if(Gate::allows('manage-pegawai')) return $next($request);
          
            abort(403, 'Anda tidak memiliki cukup hak akses');
          });
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        if (auth()->user()->kode_satker == null){
            $kartutik = KartuTikTersangka::leftJoin('satkers','kartutik_tersangkas.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_tersangkas.*','satkers.nama_satker as nama_satker')
            ->whereIn('kartutik_tersangkas.status',[2,3])
            ->get();
        }else{
            $kartutik = KartuTikTersangka::leftJoin('satkers','kartutik_tersangkas.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_tersangkas.*','satkers.nama_satker as nama_satker')
            ->where('kartutik_tersangkas.kode_satker',auth()->user()->kode_satker)
            ->whereIn('kartutik_tersangkas.status',[2,3])
            ->get();
        }

        return view('kartutik_tersangkas.index',compact('kartutik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $KartuTikTersangka = new KartuTikTersangka;
        $KartuTikTersangka->status = '1';
        $KartuTikTersangka->tipe_surat = '1';
        $KartuTikTersangka->kode_satker = auth()->user()->kode_satker;
        $KartuTikTersangka->save();

        $kartutik = KartuTikTersangka::leftJoin('satkers','kartutik_tersangkas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_tersangkas.id',$KartuTikTersangka->id)
        ->select('kartutik_tersangkas.*','satkers.nama_satker as nama_satker')
        ->get();

        $orang_tua = DB::table('kartutik_tersangkas_orangtua')->where('id_kartutik_tersangkas', $kartutik[0]->id)->get();
        $kawan = DB::table('kartutik_tersangkas_kawan')->where('id_kartutik_tersangkas', $kartutik[0]->id)->get();

        return view('kartutik_tersangkas.create', compact('kartutik','orang_tua','kawan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('save')) {
            $update = KartuTikTersangka::find($request->id);
            $update->nomor = $request->nomor;
            $update->nama_lengkap = $request->nama_lengkap;
            $update->tempat_lahir = $request->tempat_lahir;
            $update->tanggal_lahir = $request->tanggal_lahir;
            $update->jenis_kelamin = $request->jenis_kelamin;
            $update->bangsa_suku = $request->bangsa_suku;
            $update->tipe_surat = $request->tipe_surat;
            $update->bidang = $request->bidang;
            $update->kewarganegaraan = $request->kewarganegaraan;
            $update->alamat = $request->alamat;
            $update->no_ktp = $request->no_ktp;
            $update->alamat_kantor = $request->alamat_kantor;
            $update->agama = $request->agama;
            $update->pendidikan = $request->pendidikan;
            $update->pekerjaan = $request->pekerjaan;
            $update->nama_panggilan = $request->nama_panggilan;
            $update->kepartaian = $request->kepartaian;
            $update->no_hp = $request->no_hp;
            $update->kasus_posisi = $request->kasus_posisi;
            $update->latar_belakang = $request->latar_belakang;
            $update->no_skp = $request->no_skp;
            $update->tanggal_skp = $request->tanggal_skp;
            $update->status_perkawinan = $request->status_perkawinan;
            $update->putusan_pn = $request->putusan_pn;
            $update->putusan_ma = $request->putusan_ma;
            $update->putusan_pt = $request->putusan_pt;
            $update->lain_lain = $request->lain_lain;
            $update->status = '2';
            $foto = $request->file('foto');
            if ($foto) {
                $fileName = $foto->getClientOriginalName() ;
                $destinationPath = public_path().'/storage/foto' ;
                $foto->move($destinationPath,$fileName);
                $update->foto = $fileName;
            }
            $update->update();

            
            // return $request->daftar_kenalan;
            DB::table('kartutik_tersangkas_kawan')->where('id_kartutik_tersangkas', $request->id)->delete();
            foreach($request->riwayat_kawan as $key => $value) {
                DB::table('kartutik_tersangkas_kawan')->insert([
                    'nama' => $value,
                    'id_kartutik_tersangkas' => $request->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }

            DB::table('kartutik_tersangkas_orangtua')->where('id_kartutik_tersangkas', $request->id)->delete();
            foreach($request->riwayat_orang_tua as $key => $value) {
                DB::table('kartutik_tersangkas_orangtua')->insert([
                    'nama' => $value,
                    'id_kartutik_tersangkas' => $request->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }

            DB::table('kartutik_tersangkas_kawan')->where('nama', null)->delete();
            DB::table('kartutik_tersangkas_orangtua')->where('nama', null)->delete();
            return $this->edit($request->id);

        
        }elseif($request->has('finish')){
            $update = KartuTikTersangka::find($request->id);
            $update->status = '3';
            $update->update();
            return $this->edit($request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kartutik = KartuTikTersangka::leftJoin('kartutik_tersangkas_anaks','kartutik_tersangkas.id','=','kartutik_tersangkas_anaks.id_kartutik_tersangkas')
        ->leftJoin('kartutik_tersangkas_kenalan','kartutik_tersangkas.id','=','kartutik_tersangkas_kenalan.id_kartutik_tersangkas')
        ->leftJoin('kartutik_tersangkas_pendidikans','kartutik_tersangkas.id','=','kartutik_tersangkas_pendidikans.id_kartutik_tersangkas')
        ->leftJoin('kartutik_tersangkas_saudara','kartutik_tersangkas.id','=','kartutik_tersangkas_saudara.id_kartutik_tersangkas')
        ->leftJoin('satkers','kartutik_tersangkas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_tersangkas.id', $id)
        ->select('kartutik_tersangkas.*','kartutik_tersangkas_anaks.nama_anak as nama_anak','kartutik_tersangkas_kenalan.nama_kenalan as nama_kenalan','kartutik_tersangkas_pendidikans.histori_pendidikan as histori_pendidikan','satkers.nama_satker as nama_satker' ,'kartutik_tersangkas_saudara.nama_saudara as nama_saudara')
        ->get();

        return view('kartutik_tersangkas.update', compact('kartutik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $kartutik = KartuTikTersangka::leftJoin('satkers','kartutik_tersangkas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_tersangkas.id',$id)
        ->select('kartutik_tersangkas.*','satkers.nama_satker as nama_satker')
        ->get();

        $orang_tua = DB::table('kartutik_tersangkas_orangtua')->where('id_kartutik_tersangkas', $id)->get();
        $kawan = DB::table('kartutik_tersangkas_kawan')->where('id_kartutik_tersangkas', $id)->get();

        return view('kartutik_tersangkas.edit', compact('kartutik','orang_tua','kawan'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->get('status');
        $order->save();
        return redirect()->route('kartutik_tersangkas.index', [$order->id])->with('status','Status Order Berhasil Di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kartutik = KartuTikTersangka::findOrFail($id);
        $kartutik->delete();
        return Redirect::back()->with('message','Operation Successful !');
    }

    public function template(Request $request, $status, $filename = '')
    {


        $kartutik = KartuTikTersangka::leftJoin('satkers','kartutik_tersangkas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_tersangkas.id',$request->id)
        ->select('kartutik_tersangkas.*','satkers.nama_satker as nama_satker')
        ->get();
        $orang_tua = DB::table('kartutik_tersangkas_orangtua')->where('id_kartutik_tersangkas', $kartutik[0]->id)->get();
        $kawan = DB::table('kartutik_tersangkas_kawan')->where('id_kartutik_tersangkas', $kartutik[0]->id)->get();

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];		
		$defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
		
        $kertas = 'Folio';
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                'arial-cufonfonts',
            ]),
            'fontdata' => $fontData + [
                'arial' => [
                    'R' => 'ARIAL.TTF'
                ]
            ],
            'orientation' => 'P',
            'default_font_size' => 10,
            'margin_top' => 2,
            'margin_bottom' => 0,
            'margin_left' => 10,
            'margin_right' => 10,
            'mode' => 'utf-8', 
            'format' => $kertas
        ]);

        $mpdf->WriteHTML(\View::make("kartutik_tersangkas.template")->with(compact('kartutik','orang_tua','kawan')));
        // return view('kartutik_tersangkas.template', compact('kartutik','kenalan','pendidikan','anak','saudara'));

        if (empty($filename)) {
            if (!$request->encode) {
                $filename =  'kartutik_tersangka.pdf';
                $mpdf->Output($filename, 'I'); 
            }
            else {
                return base64_encode($mpdf->Output('', 'S'));
            } 
        }
        else {

            $url_upload = $this->pathStoragePangkat.'/'.$filename;
            $mpdf->Output($url_upload, 'F');
            
        }
    }
}
