<?php

namespace App\Http\Controllers;

use App\KartuTikBiodata;
use Carbon\Carbon;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;

class KartuTikBiodataController extends Controller
{

    public function __construct()
    {
        $this->middleware(function($request, $next){

            if(Gate::allows('manage-pegawai')) return $next($request);
          
            abort(403, 'Anda tidak memiliki cukup hak akses');
          });
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        if (auth()->user()->kode_satker == null){
            $kartutik = KartuTikBiodata::leftJoin('satkers','kartutik_biodatas.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_biodatas.*','satkers.nama_satker as nama_satker')
            ->whereIn('kartutik_biodatas.status',[2,3])
            ->get();
        }else{
            $kartutik = KartuTikBiodata::leftJoin('satkers','kartutik_biodatas.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_biodatas.*','satkers.nama_satker as nama_satker')
            ->where('kartutik_biodatas.kode_satker',auth()->user()->kode_satker)
            ->whereIn('kartutik_biodatas.status',[2,3])
            ->get();
        }

        return view('kartutik_biodatas.index',compact('kartutik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $KartuTikBiodata = new KartuTikBiodata;
        $KartuTikBiodata->status = '1';
        $KartuTikBiodata->kode_satker = auth()->user()->kode_satker;
        $KartuTikBiodata->save();

        $kartutik = KartuTikBiodata::leftJoin('satkers','kartutik_biodatas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_biodatas.id',$KartuTikBiodata->id)
        ->select('kartutik_biodatas.*','satkers.nama_satker as nama_satker')
        ->get();

        $kenalan = DB::table('kartutik_biodatas_kenalan')->where('id_kartutik_biodatas', $KartuTikBiodata->id)->get();
        $pendidikan = DB::table('kartutik_biodatas_pendidikans')->where('id_kartutik_biodatas', $KartuTikBiodata->id)->get();
        $anak = DB::table('kartutik_biodatas_anaks')->where('id_kartutik_biodatas', $KartuTikBiodata->id)->get();
        $saudara = DB::table('kartutik_biodatas_saudara')->where('id_kartutik_biodatas', $KartuTikBiodata->id)->get();

        return view('kartutik_biodatas.create', compact('kartutik','kenalan','pendidikan','anak','saudara'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('save')) {
            $update = KartuTikBiodata::find($request->id);
            $update->nomor = $request->nomor;
            $update->nama_lengkap = $request->nama_lengkap;
            $update->tempat_lahir = $request->tempat_lahir;
            $update->tanggal_lahir = $request->tanggal_lahir;
            $update->jenis_kelamin = $request->jenis_kelamin;
            $update->bidang = $request->bidang;
            $update->bangsa_suku = $request->bangsa_suku;
            $update->kewarganegaraan = $request->kewarganegaraan;
            $update->alamat = $request->alamat;
            $update->pendidikan = $request->pendidikan;
            $update->pekerjaan = $request->pekerjaan;
            $update->status_pernikahan = $request->status_pernikahan;
            $update->legitimasi_perkawinan = $request->legitimasi_perkawinan;
            $update->tempat_perkawinan = $request->tempat_perkawinan;
            $update->tanggal_perkawinan = $request->tanggal_perkawinan;
            $update->biografi_pekerjaan = $request->biografi_pekerjaan;
            $update->kepartaian = $request->kepartaian;
            $update->ormas_lainnya = $request->ormas_lainnya;
            $update->nama_istri_suami = $request->nama_istri_suami;
            $update->nama_ayah_kandung = $request->nama_ayah_kandung;
            $update->nama_ibu_kandung = $request->nama_ibu_kandung;
            $update->alamat_ayah_ibu = $request->alamat_ayah_ibu;
            $update->nama_ayah_mertua = $request->nama_ayah_mertua;
            $update->nama_ibu_mertua = $request->nama_ibu_mertua;
            $update->alamat_mertua = $request->alamat_mertua;
            $update->hobi_kegemaran = $request->hobi_kegemaran;
            $update->kedudukan_dimasyarakat = $request->kedudukan_dimasyarakat;
            $update->nomor_hp = $request->nomor_hp;
            $update->status = '2';
            $foto = $request->file('foto');
            if ($foto) {
                $fileName = $foto->getClientOriginalName() ;
                $destinationPath = public_path().'/storage/foto' ;
                $foto->move($destinationPath,$fileName);
                $update->foto = $fileName;
            }
            
            $update->update();

            
            // return $request->daftar_kenalan;
            DB::table('kartutik_biodatas_kenalan')->where('id_kartutik_biodatas', $request->id)->delete();
            foreach($request->daftar_kenalan as $key => $value) {
                DB::table('kartutik_biodatas_kenalan')->insert([
                    'nama_kenalan' => $value,
                    'id_kartutik_biodatas' => $request->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }

            DB::table('kartutik_biodatas_anaks')->where('id_kartutik_biodatas', $request->id)->delete();
            foreach($request->daftar_anak as $key => $value) {
                DB::table('kartutik_biodatas_anaks')->insert([
                    'nama_anak' => $value,
                    'id_kartutik_biodatas' => $request->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }

            DB::table('kartutik_biodatas_pendidikans')->where('id_kartutik_biodatas', $request->id)->delete();
            foreach($request->riwayat_pendidikan as $key => $value) {
                DB::table('kartutik_biodatas_pendidikans')->insert([
                    'histori_pendidikan' => $value,
                    'id_kartutik_biodatas' => $request->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }

            DB::table('kartutik_biodatas_saudara')->where('id_kartutik_biodatas', $request->id)->delete();
            foreach($request->daftar_saudara as $key => $value) {
                DB::table('kartutik_biodatas_saudara')->insert([
                    'nama_saudara' => $value,
                    'id_kartutik_biodatas' => $request->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }

            DB::table('kartutik_biodatas_kenalan')->where('nama_kenalan', null)->delete();
            DB::table('kartutik_biodatas_anaks')->where('nama_anak', null)->delete();
            DB::table('kartutik_biodatas_saudara')->where('nama_saudara',null)->delete();
            DB::table('kartutik_biodatas_pendidikans')->where('histori_pendidikan', null)->delete();
            return $this->edit($request->id);

        
        }elseif($request->has('finish')){
            $update = KartuTikBiodata::find($request->id);
            $update->status = '3';
            $update->update();
            return $this->edit($request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kartutik = KartuTikBiodata::leftJoin('kartutik_biodatas_anaks','kartutik_biodatas.id','=','kartutik_biodatas_anaks.id_kartutik_biodatas')
        ->leftJoin('kartutik_biodatas_kenalan','kartutik_biodatas.id','=','kartutik_biodatas_kenalan.id_kartutik_biodatas')
        ->leftJoin('kartutik_biodatas_pendidikans','kartutik_biodatas.id','=','kartutik_biodatas_pendidikans.id_kartutik_biodatas')
        ->leftJoin('kartutik_biodatas_saudara','kartutik_biodatas.id','=','kartutik_biodatas_saudara.id_kartutik_biodatas')
        ->leftJoin('satkers','kartutik_biodatas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_biodatas.id', $id)
        ->select('kartutik_biodatas.*','kartutik_biodatas_anaks.nama_anak as nama_anak','kartutik_biodatas_kenalan.nama_kenalan as nama_kenalan','kartutik_biodatas_pendidikans.histori_pendidikan as histori_pendidikan','satkers.nama_satker as nama_satker' ,'kartutik_biodatas_saudara.nama_saudara as nama_saudara')
        ->get();

        return view('kartutik_biodatas.update', compact('kartutik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $kartutik = KartuTikBiodata::leftJoin('satkers','kartutik_biodatas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_biodatas.id',$id)
        ->select('kartutik_biodatas.*','satkers.nama_satker as nama_satker')
        ->get();

        $kenalan = DB::table('kartutik_biodatas_kenalan')->where('id_kartutik_biodatas', $id)->get();
        $pendidikan = DB::table('kartutik_biodatas_pendidikans')->where('id_kartutik_biodatas', $id)->get();
        $anak = DB::table('kartutik_biodatas_anaks')->where('id_kartutik_biodatas', $id)->get();
        $saudara = DB::table('kartutik_biodatas_saudara')->where('id_kartutik_biodatas', $id)->get();

        return view('kartutik_biodatas.edit', compact('kartutik','kenalan','pendidikan','anak','saudara'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->get('status');
        $order->save();
        return redirect()->route('kartutik_biodatas.index', [$order->id])->with('status','Status Order Berhasil Di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kartutik = KartuTikBiodata::findOrFail($id);
        $kartutik->delete();
        return Redirect::back()->with('message','Operation Successful !');
    }

    public function template(Request $request, $status, $filename = '')
    {


        $kartutik = KartuTikBiodata::leftJoin('satkers','kartutik_biodatas.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_biodatas.id',$request->id)
        ->select('kartutik_biodatas.*','satkers.nama_satker as nama_satker')
        ->get();

        $kenalan = DB::table('kartutik_biodatas_kenalan')->where('id_kartutik_biodatas', $request->id)->get();
        $pendidikan = DB::table('kartutik_biodatas_pendidikans')->where('id_kartutik_biodatas', $request->id)->get();        
        $anak = DB::table('kartutik_biodatas_anaks')->where('id_kartutik_biodatas', $request->id)->get();
        $saudara = DB::table('kartutik_biodatas_saudara')->where('id_kartutik_biodatas', $request->id)->get();

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];		
		$defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
		
        $kertas = 'Folio';
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                'arial-cufonfonts',
            ]),
            'fontdata' => $fontData + [
                'arial' => [
                    'R' => 'ARIAL.TTF'
                ]
            ],
            'orientation' => 'P',
            'default_font_size' => 10,
            'margin_top' => 2,
            'margin_bottom' => 0,
            'margin_left' => 10,
            'margin_right' => 10,
            'mode' => 'utf-8', 
            'format' => $kertas
        ]);

        $mpdf->WriteHTML(\View::make("kartutik_biodatas.template")->with(compact('kartutik','kenalan','pendidikan','anak','saudara')));
        // return view('kartutik_biodatas.template', compact('kartutik','kenalan','pendidikan','anak','saudara'));

        if (empty($filename)) {
            if (!$request->encode) {
                $filename =  'kartutik_biodata.pdf';
                $mpdf->Output($filename, 'I'); 
            }
            else {
                return base64_encode($mpdf->Output('', 'S'));
            } 
        }
        else {

            $url_upload = $this->pathStoragePangkat.'/'.$filename;
            $mpdf->Output($url_upload, 'F');
            
        }
    }
}
