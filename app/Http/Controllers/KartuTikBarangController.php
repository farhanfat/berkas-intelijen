<?php

namespace App\Http\Controllers;

use App\KartuTikBarang;
use Carbon\Carbon;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;

class KartuTikBarangController extends Controller
{

    public function __construct()
    {
        $this->middleware(function($request, $next){

            if(Gate::allows('manage-pegawai')) return $next($request);
          
            abort(403, 'Anda tidak memiliki cukup hak akses');
          });
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        if (auth()->user()->kode_satker == null){
            $kartutik = KartuTikBarang::leftJoin('satkers','kartutik_barangs.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_barangs.*','satkers.nama_satker as nama_satker')
            ->whereIn('kartutik_barangs.status',[2,3])
            ->get();
        }else{
            $kartutik = KartuTikBarang::leftJoin('satkers','kartutik_barangs.kode_satker','=','satkers.kode_satker')
            ->select('kartutik_barangs.*','satkers.nama_satker as nama_satker')
            ->where('kartutik_barangs.kode_satker',auth()->user()->kode_satker)
            ->whereIn('kartutik_barangs.status',[2,3])
            ->get();
        }

        return view('kartutik_barangs.index',compact('kartutik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $KartuTikBarang = new KartuTikBarang;
        $KartuTikBarang->status = '1';
        $KartuTikBarang->kode_satker = auth()->user()->kode_satker;
        $KartuTikBarang->save();

        $kartutik = KartuTikBarang::leftJoin('satkers','kartutik_barangs.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_barangs.id',$KartuTikBarang->id)
        ->select('kartutik_barangs.*','satkers.nama_satker as nama_satker')
        ->get();

        return view('kartutik_barangs.create', compact('kartutik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('save')) {
            $update = KartuTikBarang::find($request->id);
            $update->nomor = $request->nomor;
            $update->nama_barang_cetakan = $request->nama_barang_cetakan;
            $update->penerbit = $request->penerbit;
            $update->pengarang = $request->pengarang;
            $update->waktu_peredaran = $request->waktu_peredaran;
            $update->daerah_peredaran = $request->daerah_peredaran;
            $update->pencetakan = $request->pencetakan;
            $update->nama_pimpinan_redaksi = $request->nama_pimpinan_redaksi;
            $update->alamat_penerbitan = $request->alamat_penerbitan;
            $update->alamat_pencetakan = $request->alamat_pencetakan;
            $update->jumlah_oplah = $request->jumlah_oplah;
            $update->kasus_masalah = $request->kasus_masalah;
            $update->latar_belakang = $request->latar_belakang;
            $update->tindak_kejaksaan = $request->tindak_kejaksaan;
            $update->tindak_kepolisian = $request->tindak_kepolisian;
            $update->tindak_pengadilan = $request->tindak_pengadilan;
            $update->keterangan_lain = $request->keterangan_lain;
            $update->status = '2';
            $update->update();

            
            return $this->edit($request->id);

        
        }elseif($request->has('finish')){
            $update = KartuTikBarang::find($request->id);
            $update->status = '3';
            $update->update();
            return $this->edit($request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kartutik = KartuTikBarang::leftJoin('kartutik_barangs_anaks','kartutik_barangs.id','=','kartutik_barangs_anaks.id_kartutik_barangs')
        ->leftJoin('kartutik_barangs_kenalan','kartutik_barangs.id','=','kartutik_barangs_kenalan.id_kartutik_barangs')
        ->leftJoin('kartutik_barangs_pendidikans','kartutik_barangs.id','=','kartutik_barangs_pendidikans.id_kartutik_barangs')
        ->leftJoin('kartutik_barangs_saudara','kartutik_barangs.id','=','kartutik_barangs_saudara.id_kartutik_barangs')
        ->leftJoin('satkers','kartutik_barangs.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_barangs.id', $id)
        ->select('kartutik_barangs.*','kartutik_barangs_anaks.nama_anak as nama_anak','kartutik_barangs_kenalan.nama_kenalan as nama_kenalan','kartutik_barangs_pendidikans.histori_pendidikan as histori_pendidikan','satkers.nama_satker as nama_satker' ,'kartutik_barangs_saudara.nama_saudara as nama_saudara')
        ->get();

        return view('kartutik_barangs.update', compact('kartutik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $kartutik = KartuTikBarang::leftJoin('satkers','kartutik_barangs.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_barangs.id',$id)
        ->select('kartutik_barangs.*','satkers.nama_satker as nama_satker')
        ->get();

    

        return view('kartutik_barangs.edit', compact('kartutik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->get('status');
        $order->save();
        return redirect()->route('kartutik_barangs.index', [$order->id])->with('status','Status Order Berhasil Di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kartutik = KartuTikBarang::findOrFail($id);
        $kartutik->delete();
        return Redirect::back()->with('message','Operation Successful !');
    }

    public function template(Request $request, $status, $filename = '')
    {


        $kartutik = KartuTikBarang::leftJoin('satkers','kartutik_barangs.kode_satker','=','satkers.kode_satker')
        ->where('kartutik_barangs.id',$request->id)
        ->select('kartutik_barangs.*','satkers.nama_satker as nama_satker')
        ->get();

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];		
		$defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
		
        $kertas = 'Folio';
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                'arial-cufonfonts',
            ]),
            'fontdata' => $fontData + [
                'arial' => [
                    'R' => 'ARIAL.TTF'
                ]
            ],
            'orientation' => 'P',
            'default_font_size' => 10,
            'margin_top' => 2,
            'margin_bottom' => 0,
            'margin_left' => 10,
            'margin_right' => 10,
            'mode' => 'utf-8', 
            'format' => $kertas
        ]);

        $mpdf->WriteHTML(\View::make("kartutik_barangs.template")->with(compact('kartutik')));
        // return view('kartutik_barangs.template', compact('kartutik','kenalan','pendidikan','anak','saudara'));

        if (empty($filename)) {
            if (!$request->encode) {
                $filename =  'kartutik_biodata.pdf';
                $mpdf->Output($filename, 'I'); 
            }
            else {
                return base64_encode($mpdf->Output('', 'S'));
            } 
        }
        else {

            $url_upload = $this->pathStoragePangkat.'/'.$filename;
            $mpdf->Output($url_upload, 'F');
            
        }
    }
}
